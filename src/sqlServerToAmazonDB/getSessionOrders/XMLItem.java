package sqlServerToAmazonDB.getSessionOrders;

 

/**
 * Interface for generic XML items returned from parsing. Ensures every container passed to the parser has the neccessary methods.
 * @author Connor
 *
 */
public interface XMLItem {
	public String[] getFlatFields();
	public String[] getNestedFields();
	public String[] getAttributes();
}
