package sqlServerToAmazonDB.getSessionOrders;

 
  
import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

 
@Embeddable
@Table(name = "WebServiceDTS")
public class WebServiceDTS implements XMLItem {


	@Column(name = "ID")
	private Integer ID;
	
	@Transient
	private int SessionID;
	
	@Column(name = "CustomerID")
	private String CustomerID;
	
	@Column(name = "POKEY")
	private String POKEY;
	
	@Column(name = "DocType1")
	private String DocType1;
	
	@Column(name = "DocReference1")
	private String DocReference1;
	
	@Column(name = "DocType2")
	private String DocType2;
	
	@Column(name = "DocReference2")
	private String DocReference2;
	
	@Column(name = "DocType3")
	private String DocType3;
	
	@Column(name = "DocReference3")
	private String DocReference3;
	
	@Column(name = "SourceID")
	private int SourceID;
	
	@Column(name = "Source")
	private String Source;
	
	@Column(name = "TargetID")
	private int TargetID;
	
	@Column(name = "Target")
	private String Target;
	
	@Column(name = "ECSBatchId")
	private int ECSBatchId;
	
	@Column(name = "SourceFile")
	private String SourceFile;
	
	@Column(name = "TargetFile")
	private String TargetFile;
	
	@Column(name = "ResponseFile")
	private String ResponseFile;
	
	@Column(name = "CreationDateTime")
	private Date CreationDateTime;
	
	@Column(name = "PostDateTime")
	private Date PostDateTime;
	
	@Column(name = "ResponseDateTime")
	private Date ResponseDateTime;
	
	@Column(name = "Posted")
	private int Posted;
	@Column(name = "OrderStatus")
	private String OrderStatus;
	@Column(name = "StatusDescription")
	private String StatusDescription;
	@Column(name = "SourceNbItem")
	private int SourceNbItem;
	@Column(name = " SourceTotalQuantity")
	private int SourceTotalQuantity;
	@Column(name = "TargetNbItem")
	private int TargetNbItem;
	@Column(name = "TargetTotalQuantity")
	private int TargetTotalQuantity;
	@Column(name = "ShipBatchId")
	private int ShipBatchId;
	@Column(name = "Shipped")
	private int Shipped;
	@Column(name = "ShipmentId")
	private String ShipmentId;
	@Column(name = "ShipTargetFile")
	private String ShipTargetFile;
	@Column(name = "ShipSourceFile")
	private String ShipSourceFile;
	@Column(name = "ShipResponseFile")
	private String ShipResponseFile;
	@Column(name = "ShipGetDate")
	private Date ShipGetDate;
	@Column(name = "ShipPostDate")
	private Date ShipPostDate;
	@Column(name = "ShipPkgTraceId")
	private String ShipPkgTraceId;
	@Column(name = "ShipDate")
	private Date ShipDate;
	@Column(name = "ShipMethod")
	private String ShipMethod;
	@Column(name = "ShipStatus")
	private String ShipStatus;
	@Column(name = "ShipSatusDesc")
	private String ShipSatusDesc;
	@Column(name = "Status")
	private String Status;
	@Column(name = "SatusNote")
	private String SatusNote;
	@Column(name = "StatusDate")
	private Date StatusDate;
	@Column(name = "Conversation")
	private String Conversation;
	@Column(name = "sessionLogFile")
	private String sessionLogFile;
	@Column(name = "TransactionType")
	private String TransactionType;
	
	@Transient
	private String[] flatFields = {};
	@Transient
	private String[] nestedFields = {};
	@Transient
	private String[] attributes = {"ID",
								   "SessionID",
								   "CustomerID",
								   "DocType1",
								   "DocReference1",
								   "DocReference2",
								   "DocReference3",
								   "SourceID",
								   "Source",
								   "TargetID",
								   "Target",
								   "SourceFile",
								   "CreationDateTime",
								   "PostDatetime",
								   "Posted",
								   "StatusDescription",
								   "Status",
								   "Conversation",
								   "sessionLogFile",
								   "TransactionType"};
	
	public String[] getFlatFields(){
		return flatFields;
	}
	
	public String[] getNestedFields(){
		return nestedFields;
	}
	
	public String[] getAttributes(){
		return attributes;
	}
	
	@Override
	public String toString() {
		return "--------WebServiceDTSTableClass-------\nWebServiceDTSTableClass::ID = " + ID
				+ "\nWebServiceDTSTableClass::SessionID = " + SessionID + "\nWebServiceDTSTableClass::CustomerID = "
				+ CustomerID + "\nWebServiceDTSTableClass::POKEY = " + POKEY + "\nWebServiceDTSTableClass::DocType1 = "
				+ DocType1 + "\nWebServiceDTSTableClass::DocReference1 = " + DocReference1
				+ "\nWebServiceDTSTableClass::DocType2 = " + DocType2 + "\nWebServiceDTSTableClass::DocReference2 = "
				+ DocReference2 + "\nWebServiceDTSTableClass::DocType3 = " + DocType3
				+ "\nWebServiceDTSTableClass::DocReference3 = " + DocReference3
				+ "\nWebServiceDTSTableClass::SourceID = " + SourceID + "\nWebServiceDTSTableClass::Source = " + Source
				+ "\nWebServiceDTSTableClass::TargetID = " + TargetID + "\nWebServiceDTSTableClass::Target = " + Target
				+ "\nWebServiceDTSTableClass::ECSBatchId = " + ECSBatchId + "\nWebServiceDTSTableClass::SourceFile = "
				+ SourceFile + "\nWebServiceDTSTableClass::TargetFile = " + TargetFile
				+ "\nWebServiceDTSTableClass::ResponseFile = " + ResponseFile
				+ "\nWebServiceDTSTableClass::CreationDateTime = " + CreationDateTime
				+ "\nWebServiceDTSTableClass::PostDateTime = " + PostDateTime
				+ "\nWebServiceDTSTableClass::ResponseDateTime = " + ResponseDateTime
				+ "\nWebServiceDTSTableClass::Posted = " + Posted + "\nWebServiceDTSTableClass::OrderStatus = "
				+ OrderStatus + "\nWebServiceDTSTableClass::StatusDescription = " + StatusDescription
				+ "\nWebServiceDTSTableClass::SourceNbItem = " + SourceNbItem
				+ "\nWebServiceDTSTableClass::SourceTotalQuantity = " + SourceTotalQuantity
				+ "\nWebServiceDTSTableClass::TargetNbItem = " + TargetNbItem
				+ "\nWebServiceDTSTableClass::TargetTotalQuantity = " + TargetTotalQuantity
				+ "\nWebServiceDTSTableClass::ShipBatchId = " + ShipBatchId + "\nWebServiceDTSTableClass::Shipped = "
				+ Shipped + "\nWebServiceDTSTableClass::ShipmentId = " + ShipmentId
				+ "\nWebServiceDTSTableClass::ShipTargetFile = " + ShipTargetFile
				+ "\nWebServiceDTSTableClass::ShipSourceFile = " + ShipSourceFile
				+ "\nWebServiceDTSTableClass::ShipResponseFile = " + ShipResponseFile
				+ "\nWebServiceDTSTableClass::ShipGetDate = " + ShipGetDate
				+ "\nWebServiceDTSTableClass::ShipPostDate = " + ShipPostDate
				+ "\nWebServiceDTSTableClass::ShipPkgTraceId = " + ShipPkgTraceId
				+ "\nWebServiceDTSTableClass::ShipDate = " + ShipDate + "\nWebServiceDTSTableClass::ShipMethod = "
				+ ShipMethod + "\nWebServiceDTSTableClass::ShipStatus = " + ShipStatus
				+ "\nWebServiceDTSTableClass::ShipSatusDesc = " + ShipSatusDesc + "\nWebServiceDTSTableClass::Status = "
				+ Status + "\nWebServiceDTSTableClass::SatusNote = " + SatusNote
				+ "\nWebServiceDTSTableClass::StatusDate = " + StatusDate + "\nWebServiceDTSTableClass::Conversation = "
				+ Conversation + "\nWebServiceDTSTableClass::sessionLogFile = " + sessionLogFile
				+ "\nWebServiceDTSTableClass::TransactionType = " + TransactionType
				+ "\nWebServiceDTSTableClass::flatFields = " + Arrays.toString(flatFields)
				+ "\nWebServiceDTSTableClass::nestedFields = " + Arrays.toString(nestedFields)
				+ "\nWebServiceDTSTableClass::attributes = " + Arrays.toString(attributes)
				+ "\nWebServiceDTSTableClass";
	}
	
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public Integer getSessionID() {
		return SessionID;
	}
	public void setSessionID(Integer sessionID) {
		SessionID = sessionID;
	}
	public String getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}
	public String getDocType1() {
		return DocType1;
	}
	public void setDocType1(String docType1) {
		DocType1 = docType1;
	}
	public String getDocReference1() {
		return DocReference1;
	}
	public void setDocReference1(String docReference1) {
		DocReference1 = docReference1;
	}
	public String getDocType2() {
		return DocType2;
	}
	public void setDocType2(String docType2) {
		DocType2 = docType2;
	}
	public String getDocReference2() {
		return DocReference2;
	}
	public void setDocReference2(String docReference2) {
		DocReference2 = docReference2;
	}
	public String getDocType3() {
		return DocType3;
	}
	public void setDocType3(String docType3) {
		DocType3 = docType3;
	}
	public String getDocReference3() {
		return DocReference3;
	}
	public void setDocReference3(String docReference3) {
		DocReference3 = docReference3;
	}
	public Integer getSourceID() {
		return SourceID;
	}
	public void setSourceID(Integer sourceID) {
		SourceID = sourceID;
	}
	public String getSource() {
		return Source;
	}
	public void setSource(String source) {
		Source = source;
	}
	public Integer getTargetID() {
		return TargetID;
	}
	public void setTargetID(Integer targetID) {
		TargetID = targetID;
	}
	public String getTarget() {
		return Target;
	}
	public void setTarget(String target) {
		Target = target;
	}
	public Integer getECSBatchId() {
		return ECSBatchId;
	}
	public void setECSBatchId(Integer eCSBatchId) {
		ECSBatchId = eCSBatchId;
	}
	public String getSourceFile() {
		return SourceFile;
	}
	public void setSourceFile(String sourceFile) {
		SourceFile = sourceFile;
	}
	public String getTargetFile() {
		return TargetFile;
	}
	public void setTargetFile(String targetFile) {
		TargetFile = targetFile;
	}
	public String getResponseFile() {
		return ResponseFile;
	}
	public void setResponseFile(String responseFile) {
		ResponseFile = responseFile;
	}
	public Date getCreationDateTime() {
		return CreationDateTime;
	}
	public void setCreationDateTime(Date creationDateTime) {
		CreationDateTime = creationDateTime;
	}
	public Date getPostDateTime() {
		return PostDateTime;
	}
	public void setPostDateTime(Date postDateTime) {
		PostDateTime = postDateTime;
	}
	public Date getResponseDateTime() {
		return ResponseDateTime;
	}
	public void setResponseDateTime(Date responseDateTime) {
		ResponseDateTime = responseDateTime;
	}
	public Integer getPosted() {
		return Posted;
	}
	public void setPosted(Integer posted) {
		Posted = posted;
	}
	public String getOrderStatus() {
		return OrderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		OrderStatus = orderStatus;
	}
	public String getStatusDescription() {
		return StatusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		StatusDescription = statusDescription;
	}
	public Integer getSourceNbItem() {
		return SourceNbItem;
	}
	public void setSourceNbItem(Integer sourceNbItem) {
		SourceNbItem = sourceNbItem;
	}
	public Integer getSourceTotalQuantity() {
		return SourceTotalQuantity;
	}
	public void setSourceTotalQuantity(Integer sourceTotalQuantity) {
		SourceTotalQuantity = sourceTotalQuantity;
	}
	public Integer getTargetNbItem() {
		return TargetNbItem;
	}
	public void setTargetNbItem(Integer targetNbItem) {
		TargetNbItem = targetNbItem;
	}
	public Integer getTargetTotalQuantity() {
		return TargetTotalQuantity;
	}
	public void setTargetTotalQuantity(Integer targetTotalQuantity) {
		TargetTotalQuantity = targetTotalQuantity;
	}
	public Integer getShipBatchId() {
		return ShipBatchId;
	}
	public void setShipBatchId(Integer shipBatchId) {
		ShipBatchId = shipBatchId;
	}
	public Integer getShipped() {
		return Shipped;
	}
	public void setShipped(Integer shipped) {
		Shipped = shipped;
	}
	public String getShipmentId() {
		return ShipmentId;
	}
	public void setShipmentId(String shipmentId) {
		ShipmentId = shipmentId;
	}
	public String getShipTargetFile() {
		return ShipTargetFile;
	}
	public void setShipTargetFile(String shipTargetFile) {
		ShipTargetFile = shipTargetFile;
	}
	public String getShipSourceFile() {
		return ShipSourceFile;
	}
	public void setShipSourceFile(String shipSourceFile) {
		ShipSourceFile = shipSourceFile;
	}
	public String getShipResponseFile() {
		return ShipResponseFile;
	}
	public void setShipResponseFile(String shipResponseFile) {
		ShipResponseFile = shipResponseFile;
	}
	public Date getShipGetDate() {
		return ShipGetDate;
	}
	public void setShipGetDate(Date shipGetDate) {
		ShipGetDate = shipGetDate;
	}
	public Date getShipPostDate() {
		return ShipPostDate;
	}
	public void setShipPostDate(Date shipPostDate) {
		ShipPostDate = shipPostDate;
	}
	public String getShipPkgTraceId() {
		return ShipPkgTraceId;
	}
	public void setShipPkgTraceId(String shipPkgTraceId) {
		ShipPkgTraceId = shipPkgTraceId;
	}
	public Date getShipDate() {
		return ShipDate;
	}
	public void setShipDate(Date shipDate) {
		ShipDate = shipDate;
	}
	public String getShipMethod() {
		return ShipMethod;
	}
	public void setShipMethod(String shipMethod) {
		ShipMethod = shipMethod;
	}
	public String getShipStatus() {
		return ShipStatus;
	}
	public void setShipStatus(String shipStatus) {
		ShipStatus = shipStatus;
	}
	public String getShipSatusDesc() {
		return ShipSatusDesc;
	}
	public void setShipSatusDesc(String shipSatusDesc) {
		ShipSatusDesc = shipSatusDesc;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getSatusNote() {
		return SatusNote;
	}
	public void setSatusNote(String satusNote) {
		SatusNote = satusNote;
	}
	public Date getStatusDate() {
		return StatusDate;
	}
	public void setStatusDate(Date statusDate) {
		StatusDate = statusDate;
	}
	public String getConversation() {
		return Conversation;
	}
	public void setConversation(String conversation) {
		Conversation = conversation;
	}
	public String getSessionLogFile() {
		return sessionLogFile;
	}
	public void setSessionLogFile(String sessionLogFile) {
		this.sessionLogFile = sessionLogFile;
	}
	public String getTransactionType() {
		return TransactionType;
	}
	public void setTransactionType(String transactionType) {
		TransactionType = transactionType;
	}

	public String getPOKEY() {
		return POKEY;
	}

	public void setPOKEY(String pOKEY) {
		POKEY = pOKEY;
	}
	
}
