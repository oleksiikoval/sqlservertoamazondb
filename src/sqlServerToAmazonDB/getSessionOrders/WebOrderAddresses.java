package sqlServerToAmazonDB.getSessionOrders;

 

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

 
@Embeddable
@Table(name = "WebOrderAddresses")
public class WebOrderAddresses implements XMLItem {


	@Column(name = "CustomerID")
	private String CustomerID;
	
	@Column(name = "AddressCode")
	private String AddressCode;
	@Column(name = "AddressType")
	private String AddressType;
	
	@Column(name = "POKEY")
	private String POKey;
	
	@Column(name = "LegacySessionID")
	private Integer SessionID;
	
	@Column(name = "Name")
	private String Name;
	@Column(name = "Name1")
	private String Name1;
	@Column(name = "Address1")
	private String Address1;
	@Column(name = "Address2")
	private String Address2;
	@Column(name = "Address3")
	private String Address3;
	@Column(name = "Address4")
	private String Address4;
	@Column(name = "Address5")
	private String Address5;
	@Column(name = "City")
	private String City;
	@Column(name = "Province")
	private String Province;
	@Column(name = "PostalCode")
	private String PostalCode;
	@Column(name = "Country")
	private String Country;
	@Column(name = "Phone")
	private String Phone;
	@Column(name = "AcctCode")
	private String AcctCode;
	@Column(name = "Status")
	private int Status;
	@Column(name = "PrintQueue")
	private int PrintQueue;
	@Column(name = "Misc1")
	private String Misc1;
	@Column(name = "Misc2")
	private String Misc2;
	@Column(name = "Misc3")
	private String Misc3;
	@Column(name = "CustomerCode")
	private String CustomerCode;
	@Column(name = "objID")
	private String objID;
	@Column(name = "resource_URL")
	private String resource_URL;
	@Column(name = "customerGroupOjbID")
	private String customerGroupOjbID;
	@Column(name = "customerGroupResource_uri")
	private String customerGroupResource_uri;
	@Column(name = "user_groupID")
	private String user_groupID;
	@Column(name = "user_group_Resource_uri")
	private String user_group_Resource_uri;
	@Column(name = "MapsBIUpdate")
	private int MapsBIUpdate;
	@Column(name = "Email")
	private String Email;
	@Column(name = "Company")
	private String Company;
	
	@Transient
	private String[] flatFields = {};
	
	@Transient
	private String[] nestedFields = {};
	
	@Transient
	private String[] attributes = {"CustomerID",
			   "AddressCode",
			   "AddressType",
			   "POKey",
			   "SessionID",
			   "Name",
			   "Address1",
			   "Address2",
			   "City",
			   "Province",
			   "PostalCode",
			   "Country",
			   "Phone",
			   "PrintQueue",
			   "Misc1"};

	public void setSessionID(int sessionID) {
		this.SessionID = sessionID;
	}

	public void setStatus(int status) {
		this.Status = status;
	}

	public void setPrintQueue(int printQueue) {
		this.PrintQueue = printQueue;
	}

	public void setMapsBIUpdate(int mapsBIUpdate) {
		this.MapsBIUpdate = mapsBIUpdate;
	}

	public void setFlatFields(String[] flatFields) {
		this.flatFields = flatFields;
	}

	public void setNestedFields(String[] nestedFields) {
		this.nestedFields = nestedFields;
	}

	public void setAttributes(String[] attributes) {
		this.attributes = attributes;
	}

	public String[] getFlatFields(){
		return flatFields;
	}
	
	public String[] getNestedFields(){
		return nestedFields;
	}
	
	public String[] getAttributes(){
		return attributes;
	}
	
	public String getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(String customerID) {
		this.CustomerID = customerID;
	}
	public String getAddressCode() {
		return AddressCode;
	}
	public void setAddressCode(String addressCode) {
		this.AddressCode = addressCode;
	}
	public String getAddressType() {
		return AddressType;
	}
	public void setAddressType(String addressType) {
		this.AddressType = addressType;
	}
	public String getPOKey() {
		return POKey;
	}
	public void setPOKey(String pOKey) {
		this.POKey = pOKey;
	}
	public Integer getSessionID() {
		return SessionID;
	}
	public void setSessionID(Integer sessionID) {
		this.SessionID = sessionID;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		this.Name = name;
	}
	public String getName1() {
		return Name1;
	}
	public void setName1(String name1) {
		this.Name1 = name1;
	}
	public String getAddress1() {
		return Address1;
	}
	public void setAddress1(String address1) {
		this.Address1 = address1;
	}
	public String getAddress2() {
		return Address2;
	}
	public void setAddress2(String address2) {
		this.Address2 = address2;
	}
	public String getAddress3() {
		return Address3;
	}
	public void setAddress3(String address3) {
		this.Address3 = address3;
	}
	public String getAddress4() {
		return Address4;
	}
	public void setAddress4(String address4) {
		this.Address4 = address4;
	}
	public String getAddress5() {
		return Address5;
	}
	public void setAddress5(String address5) {
		this.Address5 = address5;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		this.City = city;
	}
	public String getProvince() {
		return Province;
	}
	public void setProvince(String province) {
		this.Province = province;
	}
	public String getPostalCode() {
		return PostalCode;
	}
	public void setPostalCode(String postalCode) {
		this.PostalCode = postalCode;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		this.Country = country;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		this.Phone = phone;
	}
	public String getAcctCode() {
		return AcctCode;
	}
	public void setAcctCode(String acctCode) {
		this.AcctCode = acctCode;
	}
	public Integer getStatus() {
		return Status;
	}
	public void setStatus(Integer status) {
		this.Status = status;
	}
	public Integer getPrintQueue() {
		return PrintQueue;
	}
	public void setPrintQueue(Integer printQueue) {
		this.PrintQueue = printQueue;
	}
	public String getMisc1() {
		return Misc1;
	}
	public void setMisc1(String misc1) {
		this.Misc1 = misc1;
	}
	public String getMisc2() {
		return Misc2;
	}
	public void setMisc2(String misc2) {
		this.Misc2 = misc2;
	}
	public String getMisc3() {
		return Misc3;
	}
	public void setMisc3(String misc3) {
		this.Misc3 = misc3;
	}
	public String getCustomerCode() {
		return CustomerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.CustomerCode = customerCode;
	}
	public String getObjID() {
		return objID;
	}
	public void setObjID(String objID) {
		this.objID = objID;
	}
	public String getResource_URL() {
		return resource_URL;
	}
	public void setResource_URL(String resource_URL) {
		this.resource_URL = resource_URL;
	}
	public String getCustomerGroupOjbID() {
		return customerGroupOjbID;
	}
	public void setCustomerGroupOjbID(String customerGroupOjbID) {
		this.customerGroupOjbID = customerGroupOjbID;
	}
	public String getCustomerGroupResource_uri() {
		return customerGroupResource_uri;
	}
	public void setCustomerGroupResource_uri(String customerGroupResource_uri) {
		this.customerGroupResource_uri = customerGroupResource_uri;
	}
	public String getUser_groupID() {
		return user_groupID;
	}
	public void setUser_groupID(String user_groupID) {
		this.user_groupID = user_groupID;
	}
	public String getUser_group_Resource_uri() {
		return user_group_Resource_uri;
	}
	public void setUser_group_Resource_uri(String user_group_Resource_uri) {
		this.user_group_Resource_uri = user_group_Resource_uri;
	}
	public Integer getMapsBIUpdate() {
		return MapsBIUpdate;
	}
	public void setMapsBIUpdate(Integer mapsBIUpdate) {
		this.MapsBIUpdate = mapsBIUpdate;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		this.Email = email;
	}
	/**
	 * @return the company
	 */
	public String getCompany() {
		return Company;
	}
	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.Company = company;
	}

	@Override
	public String toString() {
		return "--------WebOrderAddressesTableClass-------\nWebOrderAddressesTableClass::CustomerID = " + CustomerID
				+ "\nWebOrderAddressesTableClass::AddressCode = " + AddressCode
				+ "\nWebOrderAddressesTableClass::AddressType = " + AddressType
				+ "\nWebOrderAddressesTableClass::POKey = " + POKey + "\nWebOrderAddressesTableClass::SessionID = "
				+ SessionID + "\nWebOrderAddressesTableClass::Name = " + Name
				+ "\nWebOrderAddressesTableClass::Name1 = " + Name1 + "\nWebOrderAddressesTableClass::Address1 = "
				+ Address1 + "\nWebOrderAddressesTableClass::Address2 = " + Address2
				+ "\nWebOrderAddressesTableClass::Address3 = " + Address3 + "\nWebOrderAddressesTableClass::Address4 = "
				+ Address4 + "\nWebOrderAddressesTableClass::Address5 = " + Address5
				+ "\nWebOrderAddressesTableClass::City = " + City + "\nWebOrderAddressesTableClass::Province = "
				+ Province + "\nWebOrderAddressesTableClass::PostalCode = " + PostalCode
				+ "\nWebOrderAddressesTableClass::Country = " + Country + "\nWebOrderAddressesTableClass::Phone = "
				+ Phone + "\nWebOrderAddressesTableClass::AcctCode = " + AcctCode
				+ "\nWebOrderAddressesTableClass::Status = " + Status + "\nWebOrderAddressesTableClass::PrintQueue = "
				+ PrintQueue + "\nWebOrderAddressesTableClass::Misc1 = " + Misc1
				+ "\nWebOrderAddressesTableClass::Misc2 = " + Misc2 + "\nWebOrderAddressesTableClass::Misc3 = " + Misc3
				+ "\nWebOrderAddressesTableClass::CustomerCode = " + CustomerCode
				+ "\nWebOrderAddressesTableClass::objID = " + objID + "\nWebOrderAddressesTableClass::resource_URL = "
				+ resource_URL + "\nWebOrderAddressesTableClass::customerGroupOjbID = " + customerGroupOjbID
				+ "\nWebOrderAddressesTableClass::customerGroupResource_uri = " + customerGroupResource_uri
				+ "\nWebOrderAddressesTableClass::user_groupID = " + user_groupID
				+ "\nWebOrderAddressesTableClass::user_group_Resource_uri = " + user_group_Resource_uri
				+ "\nWebOrderAddressesTableClass::MapsBIUpdate = " + MapsBIUpdate
				+ "\nWebOrderAddressesTableClass::Email = " + Email + "\nWebOrderAddressesTableClass::Company = "
				+ Company + "\nWebOrderAddressesTableClass::flatFields = " + Arrays.toString(flatFields)
				+ "\nWebOrderAddressesTableClass::nestedFields = " + Arrays.toString(nestedFields)
				+ "\nWebOrderAddressesTableClass::attributes = " + Arrays.toString(attributes)
				+ "\nWebOrderAddressesTableClass";
	}
	
}
