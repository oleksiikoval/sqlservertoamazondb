package sqlServerToAmazonDB.getSessionOrders;

 

import java.sql.Date;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

 
@Embeddable
@Table(name = "WebOrderDetail")
public class WebOrderDetail implements XMLItem {
	
	@Column(name = "POKEY")
	private String POKEY;
	@Column(name = "POLine")
	private Integer POLine;
	@Column(name = "Customer_PO_Line")
	private String Customer_PO_Line;
	
	@Column(name = "LegacySessionID")
	private Integer SessionID;
	
	
	@Column(name = "Sequence")
	private Integer Sequence;
	@Column(name = "OrderQty")
	private Double OrderQty;
	@Column(name = "backOrderQty")
	private Double backOrderQty;
	@Column(name = "ShipQty")
	private Double ShipQty;
	@Column(name = "ShippedQty")
	private Double ShippedQty;
	@Column(name = "InvoicedQty")
	private Double InvoicedQty;
	@Column(name = "UOM")
	private String UOM;
	@Column(name = "EDI_UOM")
	private String EDI_UOM;
	@Column(name = "UnitPrice")
	private Float UnitPrice;
	@Column(name = "RetailPrice")
	private Float RetailPrice;
	@Column(name = "discountValue")
	private Float discountValue;
	@Column(name = "SKU")
	private String SKU;
	@Column(name = "CustomerSKU")
	private String CustomerSKU;
	@Column(name = "UPC")
	private String UPC;
	@Column(name = "AlternateID")
	private String AlternateID;
	@Column(name = "Style")
	private String Style;
	@Column(name = "Colour")
	private String Colour;
	@Column(name = "Size")
	private String Size;
	@Column(name = "Inseam")
	private String Inseam;
	@Column(name = "Description")
	private String Description;
	@Column(name = "ShipDate_Detail")
	private Date ShipDate_Detail;
	@Column(name = "ExtraField1")
	private String ExtraField1;
	@Column(name = "ExtraField2")
	private String ExtraField2;
	@Column(name = "ExtraField3")
	private String ExtraField3;
	@Column(name = "ExtraField4")
	private String ExtraField4;
	@Column(name = "ExtraField5")
	private String ExtraField5;
	@Column(name = "ExtraField6")
	private String ExtraField6;
	@Column(name = "ExtraField7")
	private String ExtraField7;
	@Column(name = "ExtraField8")
	private String ExtraField8;
	@Column(name = "ExtraField9")
	private String ExtraField9;
	@Column(name = "ExtraField10")
	private String ExtraField10;
	@Column(name = "ItemStatus")
	private String ItemStatus;
	@Column(name = "ItemNotes")
	private String ItemNotes;
	@Column(name = "HubspotPosted")
	private Integer HubspotPosted;
	@Column(name = "HubspotPostedDatetime")
	private Date HubspotPostedDatetime;
	@Column(name = "HubspotPostedAttempt")
	private Integer HubspotPostedAttempt;
	@Column(name = "TrackingNumber")
	private String TrackingNumber;
	@Column(name = "SCC18")
	private String SCC18;
	@Column(name = "Pack")
	private Float Pack;
	@Column(name = "innerpack")
	private Float innerpack;
	@Column(name = "Tax")
	private Float Tax;
	@Column(name = "Tax2")
	private Float Tax2;
	@Column(name = "Ultrec")
	private String Ultrec;
	@Column(name = "VendorStyle")
	private String VendorStyle;
	@Column(name = "PackagingSpecNumber")
	private String PackagingSpecNumber;
	@Column(name = "BuyerItemNumber")
	private String BuyerItemNumber;
	@Column(name = "AssortabilityCode")
	private String AssortabilityCode;
	@Column(name = "MerchandiseCode")
	private String MerchandiseCode;
	@Column(name = "BuyerColour")
	private String BuyerColour;
	@Column(name = "Type")
	private String Type;
	@Column(name = "taxableAmount1")
	private Float taxableAmount1;
	@Column(name = "taxableAmount2")
	private Float taxableAmount2;

	@Transient
	private String[] flatFields = {};
	@Transient
	private String[] nestedFields = {};
	@Transient
	private String[] attributes =  {"POKEY",
									"POLine",
									"SessionID",
									"Sequence",
									"OrderQty",
									"ShipQty",
									"ShippedQty",
									"InvoicedQty",
									"UOM",
									"UPC",
									"Style",
									"Colour",
									"Size",
									"Description",
									"HubspotPosted",
									"HubspotPostedAttempt"};
	
	public String[] getFlatFields(){
		return flatFields;
	}
	
	public String[] getNestedFields(){
		return nestedFields;
	}
	
	public String[] getAttributes(){
		return attributes;
	}
	
	public String getPOKEY() {
		return POKEY;
	}
	public void setPOKEY(String pOKEY) {
		POKEY = pOKEY;
	}
	public Integer getPOLine() {
		return POLine;
	}
	public void setPOLine(Integer pOLine) {
		POLine = pOLine;
	}
	public String getCustomer_PO_Line() {
		return Customer_PO_Line;
	}
	public void setCustomer_PO_Line(String customer_PO_Line) {
		Customer_PO_Line = customer_PO_Line;
	}
	public Integer getSessionID() {
		return SessionID;
	}
	public void setSessionID(Integer sessionID) {
		SessionID = sessionID;
	}
	public Integer getSequence() {
		return Sequence;
	}
	public void setSequence(Integer sequence) {
		Sequence = sequence;
	}
	public Double getOrderQty() {
		return OrderQty;
	}
	public void setOrderQty(Double orderQty) {
		OrderQty = orderQty;
	}
	public Double getBackOrderQty() {
		return backOrderQty;
	}
	public void setBackOrderQty(Double backOrderQty) {
		this.backOrderQty = backOrderQty;
	}
	public Double getShipQty() {
		return ShipQty;
	}
	public void setShipQty(Double shipQty) {
		ShipQty = shipQty;
	}
	public Double getShippedQty() {
		return ShippedQty;
	}
	public void setShippedQty(Double shippedQty) {
		ShippedQty = shippedQty;
	}
	public Double getInvoicedQty() {
		return InvoicedQty;
	}
	public void setInvoicedQty(Double invoicedQty) {
		InvoicedQty = invoicedQty;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	public String getEDI_UOM() {
		return EDI_UOM;
	}
	public void setEDI_UOM(String eDI_UOM) {
		EDI_UOM = eDI_UOM;
	}
	public Float getUnitPrice() {
		return UnitPrice;
	}
	public void setUnitPrice(Float unitPrice) {
		UnitPrice = unitPrice;
	}
	public Float getRetailPrice() {
		return RetailPrice;
	}
	public void setRetailPrice(Float retailPrice) {
		RetailPrice = retailPrice;
	}
	public Float getDiscountValue() {
		return discountValue;
	}
	public void setDiscountValue(Float discountValue) {
		this.discountValue = discountValue;
	}
	public String getSKU() {
		return SKU;
	}
	public void setSKU(String sKU) {
		SKU = sKU;
	}
	public String getCustomerSKU() {
		return CustomerSKU;
	}
	public void setCustomerSKU(String customerSKU) {
		CustomerSKU = customerSKU;
	}
	public String getUPC() {
		return UPC;
	}
	public void setUPC(String uPC) {
		UPC = uPC;
	}
	public String getAlternateID() {
		return AlternateID;
	}
	public void setAlternateID(String alternateID) {
		AlternateID = alternateID;
	}
	public String getStyle() {
		return Style;
	}
	public void setStyle(String style) {
		Style = style;
	}
	public String getColour() {
		return Colour;
	}
	public void setColour(String colour) {
		Colour = colour;
	}
	public String getSize() {
		return Size;
	}
	public void setSize(String size) {
		Size = size;
	}
	public String getInseam() {
		return Inseam;
	}
	public void setInseam(String inseam) {
		Inseam = inseam;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public Date getShipDate_Detail() {
		return ShipDate_Detail;
	}
	public void setShipDate_Detail(Date shipDate_Detail) {
		ShipDate_Detail = shipDate_Detail;
	}
	public String getExtraField1() {
		return ExtraField1;
	}
	public void setExtraField1(String extraField1) {
		ExtraField1 = extraField1;
	}
	public String getExtraField2() {
		return ExtraField2;
	}
	public void setExtraField2(String extraField2) {
		ExtraField2 = extraField2;
	}
	public String getExtraField3() {
		return ExtraField3;
	}
	public void setExtraField3(String extraField3) {
		ExtraField3 = extraField3;
	}
	public String getExtraField4() {
		return ExtraField4;
	}
	public void setExtraField4(String extraField4) {
		ExtraField4 = extraField4;
	}
	public String getExtraField5() {
		return ExtraField5;
	}
	public void setExtraField5(String extraField5) {
		ExtraField5 = extraField5;
	}
	public String getExtraField6() {
		return ExtraField6;
	}
	public void setExtraField6(String extraField6) {
		ExtraField6 = extraField6;
	}
	public String getExtraField7() {
		return ExtraField7;
	}
	public void setExtraField7(String extraField7) {
		ExtraField7 = extraField7;
	}
	public String getExtraField8() {
		return ExtraField8;
	}
	public void setExtraField8(String extraField8) {
		ExtraField8 = extraField8;
	}
	public String getExtraField9() {
		return ExtraField9;
	}
	public void setExtraField9(String extraField9) {
		ExtraField9 = extraField9;
	}
	public String getExtraField10() {
		return ExtraField10;
	}
	public void setExtraField10(String extraField10) {
		ExtraField10 = extraField10;
	}
	public String getItemStatus() {
		return ItemStatus;
	}
	public void setItemStatus(String itemStatus) {
		ItemStatus = itemStatus;
	}
	public String getItemNotes() {
		return ItemNotes;
	}
	public void setItemNotes(String itemNotes) {
		ItemNotes = itemNotes;
	}
	public Integer getHubspotPosted() {
		return HubspotPosted;
	}
	public void setHubspotPosted(Integer hubspotPosted) {
		HubspotPosted = hubspotPosted;
	}
	public Date getHubspotPostedDatetime() {
		return HubspotPostedDatetime;
	}
	public void setHubspotPostedDatetime(Date hubspotPostedDatetime) {
		HubspotPostedDatetime = hubspotPostedDatetime;
	}
	public Integer getHubspotPostedAttempt() {
		return HubspotPostedAttempt;
	}
	public void setHubspotPostedAttempt(Integer hubspotPostedAttempt) {
		HubspotPostedAttempt = hubspotPostedAttempt;
	}
	public String getTrackingNumber() {
		return TrackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		TrackingNumber = trackingNumber;
	}
	public String getSCC18() {
		return SCC18;
	}
	public void setSCC18(String sCC18) {
		SCC18 = sCC18;
	}
	public Float getPack() {
		return Pack;
	}
	public void setPack(Float pack) {
		Pack = pack;
	}
	public Float getInnerpack() {
		return innerpack;
	}
	public void setInnerpack(Float innerpack) {
		this.innerpack = innerpack;
	}
	public Float getTax() {
		return Tax;
	}
	public void setTax(Float tax) {
		Tax = tax;
	}
	public Float getTax2() {
		return Tax2;
	}
	public void setTax2(Float tax2) {
		Tax2 = tax2;
	}
	public String getUltrec() {
		return Ultrec;
	}
	public void setUltrec(String ultrec) {
		Ultrec = ultrec;
	}
	public String getVendorStyle() {
		return VendorStyle;
	}
	public void setVendorStyle(String vendorStyle) {
		VendorStyle = vendorStyle;
	}
	public String getPackagingSpecNumber() {
		return PackagingSpecNumber;
	}
	public void setPackagingSpecNumber(String packagingSpecNumber) {
		PackagingSpecNumber = packagingSpecNumber;
	}
	public String getBuyerItemNumber() {
		return BuyerItemNumber;
	}
	public void setBuyerItemNumber(String buyerItemNumber) {
		BuyerItemNumber = buyerItemNumber;
	}
	public String getAssortabilityCode() {
		return AssortabilityCode;
	}
	public void setAssortabilityCode(String assortabilityCode) {
		AssortabilityCode = assortabilityCode;
	}
	public String getMerchandiseCode() {
		return MerchandiseCode;
	}
	public void setMerchandiseCode(String merchandiseCode) {
		MerchandiseCode = merchandiseCode;
	}
	public String getBuyerColour() {
		return BuyerColour;
	}
	public void setBuyerColour(String buyerColour) {
		BuyerColour = buyerColour;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public Float getTaxableAmount1() {
		return taxableAmount1;
	}
	public void setTaxableAmount1(Float taxableAmount1) {
		this.taxableAmount1 = taxableAmount1;
	}
	public Float getTaxableAmount2() {
		return taxableAmount2;
	}
	public void setTaxableAmount2(Float taxableAmount2) {
		this.taxableAmount2 = taxableAmount2;
	}


	@Override
	public String toString() {
		return "--------WebOrderDetailTableClass-------\nWebOrderDetailTableClass::POKEY = " + POKEY
				+ "\nWebOrderDetailTableClass::POLine = " + POLine + "\nWebOrderDetailTableClass::Customer_PO_Line = "
				+ Customer_PO_Line + "\nWebOrderDetailTableClass::SessionID = " + SessionID
				+ "\nWebOrderDetailTableClass::Sequence = " + Sequence + "\nWebOrderDetailTableClass::OrderQty = "
				+ OrderQty + "\nWebOrderDetailTableClass::backOrderQty = " + backOrderQty
				+ "\nWebOrderDetailTableClass::ShipQty = " + ShipQty + "\nWebOrderDetailTableClass::ShippedQty = "
				+ ShippedQty + "\nWebOrderDetailTableClass::InvoicedQty = " + InvoicedQty
				+ "\nWebOrderDetailTableClass::UOM = " + UOM + "\nWebOrderDetailTableClass::EDI_UOM = " + EDI_UOM
				+ "\nWebOrderDetailTableClass::UnitPrice = " + UnitPrice + "\nWebOrderDetailTableClass::RetailPrice = "
				+ RetailPrice + "\nWebOrderDetailTableClass::discountValue = " + discountValue
				+ "\nWebOrderDetailTableClass::SKU = " + SKU + "\nWebOrderDetailTableClass::CustomerSKU = "
				+ CustomerSKU + "\nWebOrderDetailTableClass::UPC = " + UPC
				+ "\nWebOrderDetailTableClass::AlternateID = " + AlternateID + "\nWebOrderDetailTableClass::Style = "
				+ Style + "\nWebOrderDetailTableClass::Colour = " + Colour + "\nWebOrderDetailTableClass::Size = "
				+ Size + "\nWebOrderDetailTableClass::Inseam = " + Inseam + "\nWebOrderDetailTableClass::Description = "
				+ Description + "\nWebOrderDetailTableClass::ShipDate_Detail = " + ShipDate_Detail
				+ "\nWebOrderDetailTableClass::ExtraField1 = " + ExtraField1
				+ "\nWebOrderDetailTableClass::ExtraField2 = " + ExtraField2
				+ "\nWebOrderDetailTableClass::ExtraField3 = " + ExtraField3
				+ "\nWebOrderDetailTableClass::ExtraField4 = " + ExtraField4
				+ "\nWebOrderDetailTableClass::ExtraField5 = " + ExtraField5
				+ "\nWebOrderDetailTableClass::ExtraField6 = " + ExtraField6
				+ "\nWebOrderDetailTableClass::ExtraField7 = " + ExtraField7
				+ "\nWebOrderDetailTableClass::ExtraField8 = " + ExtraField8
				+ "\nWebOrderDetailTableClass::ExtraField9 = " + ExtraField9
				+ "\nWebOrderDetailTableClass::ExtraField10 = " + ExtraField10
				+ "\nWebOrderDetailTableClass::ItemStatus = " + ItemStatus + "\nWebOrderDetailTableClass::ItemNotes = "
				+ ItemNotes + "\nWebOrderDetailTableClass::HubspotPosted = " + HubspotPosted
				+ "\nWebOrderDetailTableClass::HubspotPostedDatetime = " + HubspotPostedDatetime
				+ "\nWebOrderDetailTableClass::HubspotPostedAttempt = " + HubspotPostedAttempt
				+ "\nWebOrderDetailTableClass::TrackingNumber = " + TrackingNumber
				+ "\nWebOrderDetailTableClass::SCC18 = " + SCC18 + "\nWebOrderDetailTableClass::Pack = " + Pack
				+ "\nWebOrderDetailTableClass::innerpack = " + innerpack + "\nWebOrderDetailTableClass::Tax = " + Tax
				+ "\nWebOrderDetailTableClass::Tax2 = " + Tax2 + "\nWebOrderDetailTableClass::Ultrec = " + Ultrec
				+ "\nWebOrderDetailTableClass::VendorStyle = " + VendorStyle
				+ "\nWebOrderDetailTableClass::PackagingSpecNumber = " + PackagingSpecNumber
				+ "\nWebOrderDetailTableClass::BuyerItemNumber = " + BuyerItemNumber
				+ "\nWebOrderDetailTableClass::AssortabilityCode = " + AssortabilityCode
				+ "\nWebOrderDetailTableClass::MerchandiseCode = " + MerchandiseCode
				+ "\nWebOrderDetailTableClass::BuyerColour = " + BuyerColour + "\nWebOrderDetailTableClass::Type = "
				+ Type + "\nWebOrderDetailTableClass::taxableAmount1 = " + taxableAmount1
				+ "\nWebOrderDetailTableClass::taxableAmount2 = " + taxableAmount2
				+  "\nWebOrderDetailTableClass::flatFields = " + Arrays.toString(flatFields)
				+ "\nWebOrderDetailTableClass::nestedFields = " + Arrays.toString(nestedFields)
				+ "\nWebOrderDetailTableClass::attributes = " + Arrays.toString(attributes)
				+ "\nWebOrderDetailTableClass";
	}
	
	
}
