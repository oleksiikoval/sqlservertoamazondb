package sqlServerToAmazonDB.getSessionOrders;

 
  
import java.sql.Date;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
    
@Embeddable
@Table(name = "WebOrderHeader")
public class WebOrderHeader implements XMLItem {

	@Column(name = "POKEY")
	private String POKEY;
	

	@Column(name = "LegacySessionID")
	private Integer SessionID;
	
	@Column(name = "CustomerID")
	private String CustomerID;
	@Column(name = "WebOrderNumber")
	private String WebOrderNumber;
	@Column(name = "WebOrderID")
	private String WebOrderID;
	
	@Column(name = "SalesOrderId")
	private String SalesOrderId;
	@Column(name = "InvoiceNumber")
	private String InvoiceNumber;
	@Column(name = "OrderDate")
	private Date OrderDate;
	@Column(name = "CreatedDate")
	private Date CreatedDate;
	@Column(name = "InvoiceDate")
	private Date InvoiceDate;
	@Column(name = "OrderStatus")
	private String OrderStatus;
	@Column(name = "Fulfilment_Status")
	private String Fulfilment_Status;
	@Column(name = "FirstName")
	private String FirstName;
	@Column(name = "LastName")
	private String LastName;
	@Column(name = "Salutation")
	private String Salutation;
	@Column(name = "Webstore")
	private String Webstore;
	@Column(name = "OrderType")
	private String OrderType;
	@Column(name = "Currency")
	private String Currency;
	@Column(name = "Vendor")
	private String Vendor;
	@Column(name = "AppId")
	private String AppId;
	@Column(name = "Deptno")
	private String Deptno;
	@Column(name = "EDI_DeptNo")
	private String EDI_DeptNo;
	@Column(name = "ShipTo")
	private String ShipTo;
	@Column(name = "ShipMethod")
	private String ShipMethod;
	@Column(name = "ShipMethodDesc")
	private String ShipMethodDesc;
	@Column(name = "TrackingNumber")
	private String TrackingNumber;
	@Column(name = "ShipmentId")
	private String ShipmentId;
	@Column(name = "ShipmentWeightUOM")
	private String ShipmentWeightUOM;
	@Column(name = "UnitShipped")
	private Float UnitShipped;
	@Column(name = "ShipmentWeight")
	private Float ShipmentWeight;
	@Column(name = "BOL")
	private String BOL;
	@Column(name = "ShipDate")
	private Date ShipDate;
	@Column(name = "ShippingCost")
	private Float ShippingCost;
	@Column(name = "EventCode")
	private String EventCode;
	@Column(name = "BlanketNo")
	private String BlanketNo;
	@Column(name = "TermType")
	private String TermType;
	@Column(name = "TermBasisDate")
	private String TermBasisDate;
	@Column(name = "TermsAmt")
	private Double TermsAmt;
	@Column(name = "TermsDiscDue")
	private int TermsDiscDue;
	@Column(name = "TermsNetDue")
	private int TermsNetDue;
	@Column(name = "RequestShipDate")
	private Date RequestShipDate;
	@Column(name = "ShipNotB4Date")
	private Date ShipNotB4Date;
	@Column(name = "CancelAfterDate")
	private Date CancelAfterDate;
	@Column(name = "ReqDeliveryDate")
	private Date ReqDeliveryDate;
	@Column(name = "InvAlias")
	private String InvAlias;
	@Column(name = "Promo")
	private String Promo;
	@Column(name = "Type")
	private String Type;
	@Column(name = "ImportDate")
	private Date ImportDate;
	@Column(name = "BackOrder")
	private int BackOrder;
	@Column(name = "ShipNow")
	private int ShipNow;
	@Column(name = "PrintNow")
	private int PrintNow;
	@Column(name = "ExportNow")
	private int ExportNow;
	@Column(name = "Exported")
	private int Exported;
	@Column(name = "AccExportNow")
	private int AccExportNow;
	@Column(name = "AccExported")
	private int AccExported;
	@Column(name = "ContactName")
	private String ContactName;
	@Column(name = "ContactPhone")
	private String ContactPhone;
	@Column(name = "Cancelled")
	private int Cancelled;
	@Column(name = "ExtraField1")
	private String ExtraField1;
	@Column(name = "ExtraField2")
	private String ExtraField2;
	@Column(name = "ExtraField3")
	private String ExtraField3;
	@Column(name = "ExtraField4")
	private String ExtraField4;
	@Column(name = "ExtraField5")
	private String ExtraField5;
	@Column(name = "ExtraField6")
	private String ExtraField6;
	@Column(name = "ExtraField7")
	private String ExtraField7;
	@Column(name = "ExtraField8")
	private String ExtraField8;
	@Column(name = "ExtraField9")
	private String ExtraField9;
	@Column(name = "ExtraField10")
	private String ExtraField10;
	@Column(name = "ArchiveNow")
	private int ArchiveNow;
	@Column(name = "Confirmed")
	private int Confirmed;
	@Column(name = "ChangesExported")
	private int ChangesExported;
	@Column(name = "PriceList")
	private String PriceList;
	@Column(name = "ExportDate")
	private Date ExportDate;
	@Column(name = "AckSentStatus")
	private int AckSentStatus;
	@Column(name = "AckSentDate")
	private Date AckSentDate;
	@Column(name = "EmailAddress")
	private String EmailAddress;
	@Column(name = "IPAddress")
	private String IPAddress;
	@Column(name = "OrderNotes")
	private String OrderNotes;
	@Column(name = "GiftNotes")
	private String GiftNotes;
	@Column(name = "Subtotal")
	private Float Subtotal;
	@Column(name = "Discount")
	private Float Discount;
	@Column(name = "Tax")
	private Float Tax;
	@Column(name = "Tax2")
	private Float Tax2;
	@Column(name = "TaxRate")
	private Float TaxRate;
	@Column(name = "TaxRate2")
	private Float TaxRate2;
	@Column(name = "TaxCode")
	private String TaxCode;
	@Column(name = "TaxCode2")
	private String TaxCode2;
	@Column(name = "GrandTotal")
	private Float GrandTotal;
	@Column(name = "Residential")
	private int Residential;
	@Column(name = "Source")
	private String Source;
	@Column(name = "ConfigID")
	private String ConfigID;
	@Column(name = "Warehouse_location")
	private String Warehouse_location;
	@Column(name = "Credit_card_type")
	private String Credit_card_type;
	@Column(name = "Credit_card_name")
	private String Credit_card_name;
	@Column(name = "Credit_card_number")
	private String Credit_card_number;
	@Column(name = "Credit_card_expiry")
	private String Credit_card_expiry;
	@Column(name = "Credit_card_code")
	private String Credit_card_code;
	@Column(name = "IsShipmentOrder")
	private int IsShipmentOrder;
	@Column(name = "Conversation")
	private String Conversation;
	@Column(name = "Payment_status")
	private String Payment_status;
	@Column(name = "updatedDate")
	private Date updatedDate;
	@Column(name = "FOB")
	private String FOB;
	@Column(name = "Ultrec")
	private String Ultrec;
	@Column(name = "PercentageDiscount")
	private Float PercentageDiscount;
	@Column(name = "CouponCode")
	private String CouponCode;
	@Column(name = "EDI_FOB")
	private String EDI_FOB;
	@Column(name = "ProductType")
	private String ProductType;
	@Column(name = "InternalControlNumber")
	private String InternalControlNumber;
	@Column(name = "Payee_identification")
	private String Payee_identification;
	@Column(name = "customer_order_number")
	private String customer_order_number;
	@Column(name = "Subday_number")
	private String Subday_number;
	@Column(name = "TermsDescription")
	private String TermsDescription;
	@Column(name = "Sender_Qual")
	private String Sender_Qual;
	@Column(name = "Sender_ID")
	private String Sender_ID;
	@Column(name = "Sender_GS")
	private String Sender_GS;
	@Column(name = "EDIDesktop_Retailer")
	private String EDIDesktop_Retailer;
	@Column(name = "ProNumber")
	private String ProNumber;
	@Column(name = "ShippingNotes")
	private String ShippingNotes;
	@Column(name = "Three3PLOrderID")
	private String Three3PLOrderID;
	@Column(name = "ShippedCost")
	private Float ShippedCost;
	@Column(name = "ApptNo")
	private String ApptNo;
	@Column(name = "objID")
	private String objID;
	@Column(name = "resource_URL")
	private String resource_URL;
	@Column(name = "salesRep")
	private String salesRep;
	@Column(name = "SP1Split")
	private int SP1Split;
	@Column(name = "udfValue")
	private String udfValue;
	@Column(name = "UDFCode")
	private String UDFCode;
	@Column(name = "OrderCategory")
	private String OrderCategory;
	
	@Transient
	private String[] flatFields = {};
	@Transient
	private String[] nestedFields = {};
	@Transient
	private String[] attributes = {"POKEY",
								   "SessionID",
								   "CustomerID",
								   "WebOrderNumber",
								   "WebOrderID",
								   "SalesOrderID",
								   "InvoiceNumber",
								   "OrderDate",
								   "CreatedDate",
								   "OrderStatus",
								   "FirstName",
								   "LastName",
								   "Webstore",
								   "Currency",
								   "Vendor",
								   "Deptno",
								   "EDI_DeptNo",
								   "ShipTo",
								   "ShipMethod",
								   "ShipMethodDesc",
								   "BackOrder",
								   "ShipNow",
								   "PrintNow",
								   "ExportNow",
								   "Exported",
								   "AccExportNow",
								   "AccExported",
								   "Cancelled",
								   "ArchiveNow",
								   "Confirmed",
								   "ChangesExported",
								   "AckSentStatus",
								   "OrderNotes",
								   "Residential",
								   "IsShipmentOrder",
								   "Conversation",
								   "UpdatedDate",
								   "customer_order_number",
								   "ShippingNotes",
								   "SP1Split"};
	
	public String[] getFlatFields(){
		return flatFields;
	}
	
	public String[] getNestedFields(){
		return nestedFields;
	}
	
	public String[] getAttributes(){
		return attributes;
	}
	
	public String getPOKEY() {
		return POKEY;
	}
	public void setPOKEY(String pOKEY) {
		POKEY = pOKEY;
	}
	public Integer getSessionID() {
		return SessionID;
	}
	public void setSessionID(Integer sessionID) {
		SessionID = sessionID;
	}
	public String getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}
	public String getWebOrderNumber() {
		return WebOrderNumber;
	}
	public void setWebOrderNumber(String webOrderNumber) {
		WebOrderNumber = webOrderNumber;
	}
	public String getWebOrderID() {
		return WebOrderID;
	}
	public void setWebOrderID(String webOrderID) {
		WebOrderID = webOrderID;
	}
	public String getSalesOrderId() {
		return SalesOrderId;
	}
	public void setSalesOrderId(String salesOrderId) {
		SalesOrderId = salesOrderId;
	}
	public String getInvoiceNumber() {
		return InvoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		InvoiceNumber = invoiceNumber;
	}
	public Date getOrderDate() {
		return OrderDate;
	}
	public void setOrderDate(Date orderDate) {
		OrderDate = orderDate;
	}
	public Date getCreatedDate() {
		return CreatedDate;
	}
	public void setCreatedDate(Date createdDate) {
		CreatedDate = createdDate;
	}
	public Date getInvoiceDate() {
		return InvoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		InvoiceDate = invoiceDate;
	}
	public String getOrderStatus() {
		return OrderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		OrderStatus = orderStatus;
	}
	public String getFulfilment_Status() {
		return Fulfilment_Status;
	}
	public void setFulfilment_Status(String fulfilment_Status) {
		Fulfilment_Status = fulfilment_Status;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getSalutation() {
		return Salutation;
	}
	public void setSalutation(String salutation) {
		Salutation = salutation;
	}
	public String getWebstore() {
		return Webstore;
	}
	public void setWebstore(String webstore) {
		Webstore = webstore;
	}
	public String getOrderType() {
		return OrderType;
	}
	public void setOrderType(String orderType) {
		OrderType = orderType;
	}
	public String getCurrency() {
		return Currency;
	}
	public void setCurrency(String currency) {
		Currency = currency;
	}
	public String getVendor() {
		return Vendor;
	}
	public void setVendor(String vendor) {
		Vendor = vendor;
	}
	public String getAppId() {
		return AppId;
	}
	public void setAppId(String appId) {
		AppId = appId;
	}
	public String getDeptno() {
		return Deptno;
	}
	public void setDeptno(String deptno) {
		Deptno = deptno;
	}
	public String getEDI_DeptNo() {
		return EDI_DeptNo;
	}
	public void setEDI_DeptNo(String eDI_DeptNo) {
		EDI_DeptNo = eDI_DeptNo;
	}
	public String getShipTo() {
		return ShipTo;
	}
	public void setShipTo(String shipTo) {
		ShipTo = shipTo;
	}
	public String getShipMethod() {
		return ShipMethod;
	}
	public void setShipMethod(String shipMethod) {
		ShipMethod = shipMethod;
	}
	public String getShipMethodDesc() {
		return ShipMethodDesc;
	}
	public void setShipMethodDesc(String shipMethodDesc) {
		ShipMethodDesc = shipMethodDesc;
	}
	public String getTrackingNumber() {
		return TrackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		TrackingNumber = trackingNumber;
	}
	public String getShipmentId() {
		return ShipmentId;
	}
	public void setShipmentId(String shipmentId) {
		ShipmentId = shipmentId;
	}
	public String getShipmentWeightUOM() {
		return ShipmentWeightUOM;
	}
	public void setShipmentWeightUOM(String shipmentWeightUOM) {
		ShipmentWeightUOM = shipmentWeightUOM;
	}
	public Float getUnitShipped() {
		return UnitShipped;
	}
	public void setUnitShipped(Float unitShipped) {
		UnitShipped = unitShipped;
	}
	public Float getShipmentWeight() {
		return ShipmentWeight;
	}
	public void setShipmentWeight(Float shipmentWeight) {
		ShipmentWeight = shipmentWeight;
	}
	public String getBOL() {
		return BOL;
	}
	public void setBOL(String bOL) {
		BOL = bOL;
	}
	public Date getShipDate() {
		return ShipDate;
	}
	public void setShipDate(Date shipDate) {
		ShipDate = shipDate;
	}
	public Float getShippingCost() {
		return ShippingCost;
	}
	public void setShippingCost(Float shippingCost) {
		ShippingCost = shippingCost;
	}
	public String getEventCode() {
		return EventCode;
	}
	public void setEventCode(String eventCode) {
		EventCode = eventCode;
	}
	public String getBlanketNo() {
		return BlanketNo;
	}
	public void setBlanketNo(String blanketNo) {
		BlanketNo = blanketNo;
	}
	public String getTermType() {
		return TermType;
	}
	public void setTermType(String termType) {
		TermType = termType;
	}
	public String getTermBasisDate() {
		return TermBasisDate;
	}
	public void setTermBasisDate(String termBasisDate) {
		TermBasisDate = termBasisDate;
	}
	public Double getTermsAmt() {
		return TermsAmt;
	}
	public void setTermsAmt(Double termsAmt) {
		TermsAmt = termsAmt;
	}
	public Integer getTermsDiscDue() {
		return TermsDiscDue;
	}
	public void setTermsDiscDue(Integer termsDiscDue) {
		TermsDiscDue = termsDiscDue;
	}
	public Integer getTermsNetDue() {
		return TermsNetDue;
	}
	public void setTermsNetDue(Integer termsNetDue) {
		TermsNetDue = termsNetDue;
	}
	public Date getRequestShipDate() {
		return RequestShipDate;
	}
	public void setRequestShipDate(Date requestShipDate) {
		RequestShipDate = requestShipDate;
	}
	public Date getShipNotB4Date() {
		return ShipNotB4Date;
	}
	public void setShipNotB4Date(Date shipNotB4Date) {
		ShipNotB4Date = shipNotB4Date;
	}
	public Date getCancelAfterDate() {
		return CancelAfterDate;
	}
	public void setCancelAfterDate(Date cancelAfterDate) {
		CancelAfterDate = cancelAfterDate;
	}
	public Date getReqDeliveryDate() {
		return ReqDeliveryDate;
	}
	public void setReqDeliveryDate(Date reqDeliveryDate) {
		ReqDeliveryDate = reqDeliveryDate;
	}
	public String getInvAlias() {
		return InvAlias;
	}
	public void setInvAlias(String invAlias) {
		InvAlias = invAlias;
	}
	public String getPromo() {
		return Promo;
	}
	public void setPromo(String promo) {
		Promo = promo;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public Date getImportDate() {
		return ImportDate;
	}
	public void setImportDate(Date importDate) {
		ImportDate = importDate;
	}
	public Integer getBackOrder() {
		return BackOrder;
	}
	public void setBackOrder(Integer backOrder) {
		BackOrder = backOrder;
	}
	public Integer getShipNow() {
		return ShipNow;
	}
	public void setShipNow(Integer shipNow) {
		ShipNow = shipNow;
	}
	public Integer getPrintNow() {
		return PrintNow;
	}
	public void setPrintNow(Integer printNow) {
		PrintNow = printNow;
	}
	public Integer getExportNow() {
		return ExportNow;
	}
	public void setExportNow(Integer exportNow) {
		ExportNow = exportNow;
	}
	public Integer getExported() {
		return Exported;
	}
	public void setExported(Integer exported) {
		Exported = exported;
	}
	public Integer getAccExportNow() {
		return AccExportNow;
	}
	public void setAccExportNow(Integer accExportNow) {
		AccExportNow = accExportNow;
	}
	public Integer getAccExported() {
		return AccExported;
	}
	public void setAccExported(Integer accExported) {
		AccExported = accExported;
	}
	public String getContactName() {
		return ContactName;
	}
	public void setContactName(String contactName) {
		ContactName = contactName;
	}
	public String getContactPhone() {
		return ContactPhone;
	}
	public void setContactPhone(String contactPhone) {
		ContactPhone = contactPhone;
	}
	public Integer getCancelled() {
		return Cancelled;
	}
	public void setCancelled(Integer cancelled) {
		Cancelled = cancelled;
	}
	public String getExtraField1() {
		return ExtraField1;
	}
	public void setExtraField1(String extraField1) {
		ExtraField1 = extraField1;
	}
	public String getExtraField2() {
		return ExtraField2;
	}
	public void setExtraField2(String extraField2) {
		ExtraField2 = extraField2;
	}
	public String getExtraField3() {
		return ExtraField3;
	}
	public void setExtraField3(String extraField3) {
		ExtraField3 = extraField3;
	}
	public String getExtraField4() {
		return ExtraField4;
	}
	public void setExtraField4(String extraField4) {
		ExtraField4 = extraField4;
	}
	public String getExtraField5() {
		return ExtraField5;
	}
	public void setExtraField5(String extraField5) {
		ExtraField5 = extraField5;
	}
	public String getExtraField6() {
		return ExtraField6;
	}
	public void setExtraField6(String extraField6) {
		ExtraField6 = extraField6;
	}
	public String getExtraField7() {
		return ExtraField7;
	}
	public void setExtraField7(String extraField7) {
		ExtraField7 = extraField7;
	}
	public String getExtraField8() {
		return ExtraField8;
	}
	public void setExtraField8(String extraField8) {
		ExtraField8 = extraField8;
	}
	public String getExtraField9() {
		return ExtraField9;
	}
	public void setExtraField9(String extraField9) {
		ExtraField9 = extraField9;
	}
	public String getExtraField10() {
		return ExtraField10;
	}
	public void setExtraField10(String extraField10) {
		ExtraField10 = extraField10;
	}
	public Integer getArchiveNow() {
		return ArchiveNow;
	}
	public void setArchiveNow(Integer archiveNow) {
		ArchiveNow = archiveNow;
	}
	public Integer getConfirmed() {
		return Confirmed;
	}
	public void setConfirmed(Integer confirmed) {
		Confirmed = confirmed;
	}
	public Integer getChangesExported() {
		return ChangesExported;
	}
	public void setChangesExported(Integer changesExported) {
		ChangesExported = changesExported;
	}
	public String getPriceList() {
		return PriceList;
	}
	public void setPriceList(String priceList) {
		PriceList = priceList;
	}
	public Date getExportDate() {
		return ExportDate;
	}
	public void setExportDate(Date exportDate) {
		ExportDate = exportDate;
	}
	public Integer getAckSentStatus() {
		return AckSentStatus;
	}
	public void setAckSentStatus(Integer ackSentStatus) {
		AckSentStatus = ackSentStatus;
	}
	public Date getAckSentDate() {
		return AckSentDate;
	}
	public void setAckSentDate(Date ackSentDate) {
		AckSentDate = ackSentDate;
	}
	public String getEmailAddress() {
		return EmailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}
	public String getIPAddress() {
		return IPAddress;
	}
	public void setIPAddress(String iPAddress) {
		IPAddress = iPAddress;
	}
	public String getOrderNotes() {
		return OrderNotes;
	}
	public void setOrderNotes(String orderNotes) {
		OrderNotes = orderNotes;
	}
	public String getGiftNotes() {
		return GiftNotes;
	}
	public void setGiftNotes(String giftNotes) {
		GiftNotes = giftNotes;
	}
	public Float getSubtotal() {
		return Subtotal;
	}
	public void setSubtotal(Float subtotal) {
		Subtotal = subtotal;
	}
	public Float getDiscount() {
		return Discount;
	}
	public void setDiscount(Float discount) {
		Discount = discount;
	}
	public Float getTax() {
		return Tax;
	}
	public void setTax(Float tax) {
		Tax = tax;
	}
	public Float getTax2() {
		return Tax2;
	}
	public void setTax2(Float tax2) {
		Tax2 = tax2;
	}
	public Float getTaxRate() {
		return TaxRate;
	}
	public void setTaxRate(Float taxRate) {
		TaxRate = taxRate;
	}
	public Float getTaxRate2() {
		return TaxRate2;
	}
	public void setTaxRate2(Float taxRate2) {
		TaxRate2 = taxRate2;
	}
	public String getTaxCode() {
		return TaxCode;
	}
	public void setTaxCode(String taxCode) {
		TaxCode = taxCode;
	}
	public String getTaxCode2() {
		return TaxCode2;
	}
	public void setTaxCode2(String taxCode2) {
		TaxCode2 = taxCode2;
	}
	public Float getGrandTotal() {
		return GrandTotal;
	}
	public void setGrandTotal(Float grandTotal) {
		GrandTotal = grandTotal;
	}
	public Integer getResidential() {
		return Residential;
	}
	public void setResidential(Integer residential) {
		Residential = residential;
	}
	public String getSource() {
		return Source;
	}
	public void setSource(String source) {
		Source = source;
	}
	public String getConfigID() {
		return ConfigID;
	}
	public void setConfigID(String configID) {
		ConfigID = configID;
	}
	public String getWarehouse_location() {
		return Warehouse_location;
	}
	public void setWarehouse_location(String warehouse_location) {
		Warehouse_location = warehouse_location;
	}
	public String getCredit_card_type() {
		return Credit_card_type;
	}
	public void setCredit_card_type(String credit_card_type) {
		Credit_card_type = credit_card_type;
	}
	public String getCredit_card_name() {
		return Credit_card_name;
	}
	public void setCredit_card_name(String credit_card_name) {
		Credit_card_name = credit_card_name;
	}
	public String getCredit_card_number() {
		return Credit_card_number;
	}
	public void setCredit_card_number(String credit_card_number) {
		Credit_card_number = credit_card_number;
	}
	public String getCredit_card_expiry() {
		return Credit_card_expiry;
	}
	public void setCredit_card_expiry(String credit_card_expiry) {
		Credit_card_expiry = credit_card_expiry;
	}
	public String getCredit_card_code() {
		return Credit_card_code;
	}
	public void setCredit_card_code(String credit_card_code) {
		Credit_card_code = credit_card_code;
	}
	public Integer getIsShipmentOrder() {
		return IsShipmentOrder;
	}
	public void setIsShipmentOrder(Integer isShipmentOrder) {
		IsShipmentOrder = isShipmentOrder;
	}
	public String getConversation() {
		return Conversation;
	}
	public void setConversation(String conversation) {
		Conversation = conversation;
	}
	public String getPayment_status() {
		return Payment_status;
	}
	public void setPayment_status(String payment_status) {
		Payment_status = payment_status;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getFOB() {
		return FOB;
	}
	public void setFOB(String fOB) {
		FOB = fOB;
	}
	public String getUltrec() {
		return Ultrec;
	}
	public void setUltrec(String ultrec) {
		Ultrec = ultrec;
	}
	public Float getPercentageDiscount() {
		return PercentageDiscount;
	}
	public void setPercentageDiscount(Float percentageDiscount) {
		PercentageDiscount = percentageDiscount;
	}
	public String getCouponCode() {
		return CouponCode;
	}
	public void setCouponCode(String couponCode) {
		CouponCode = couponCode;
	}
	public String getEDI_FOB() {
		return EDI_FOB;
	}
	public void setEDI_FOB(String eDI_FOB) {
		EDI_FOB = eDI_FOB;
	}
	public String getProductType() {
		return ProductType;
	}
	public void setProductType(String productType) {
		ProductType = productType;
	}
	public String getInternalControlNumber() {
		return InternalControlNumber;
	}
	public void setInternalControlNumber(String internalControlNumber) {
		InternalControlNumber = internalControlNumber;
	}
	public String getPayee_identification() {
		return Payee_identification;
	}
	public void setPayee_identification(String payee_identification) {
		Payee_identification = payee_identification;
	}
	public String getCustomer_order_number() {
		return customer_order_number;
	}
	public void setCustomer_order_number(String customer_order_number) {
		this.customer_order_number = customer_order_number;
	}
	public String getSubday_number() {
		return Subday_number;
	}
	public void setSubday_number(String subday_number) {
		Subday_number = subday_number;
	}
	public String getTermsDescription() {
		return TermsDescription;
	}
	public void setTermsDescription(String termsDescription) {
		TermsDescription = termsDescription;
	}
	public String getSender_Qual() {
		return Sender_Qual;
	}
	public void setSender_Qual(String sender_Qual) {
		Sender_Qual = sender_Qual;
	}
	public String getSender_ID() {
		return Sender_ID;
	}
	public void setSender_ID(String sender_ID) {
		Sender_ID = sender_ID;
	}
	public String getSender_GS() {
		return Sender_GS;
	}
	public void setSender_GS(String sender_GS) {
		Sender_GS = sender_GS;
	}
	public String getEDIDesktop_Retailer() {
		return EDIDesktop_Retailer;
	}
	public void setEDIDesktop_Retailer(String eDIDesktop_Retailer) {
		EDIDesktop_Retailer = eDIDesktop_Retailer;
	}
	public String getProNumber() {
		return ProNumber;
	}
	public void setProNumber(String proNumber) {
		ProNumber = proNumber;
	}
	public String getShippingNotes() {
		return ShippingNotes;
	}
	public void setShippingNotes(String shippingNotes) {
		ShippingNotes = shippingNotes;
	}
	public String getThree3PLOrderID() {
		return Three3PLOrderID;
	}
	public void setThree3PLOrderID(String three3plOrderID) {
		Three3PLOrderID = three3plOrderID;
	}
	public Float getShippedCost() {
		return ShippedCost;
	}
	public void setShippedCost(Float shippedCost) {
		ShippedCost = shippedCost;
	}
	public String getApptNo() {
		return ApptNo;
	}
	public void setApptNo(String apptNo) {
		ApptNo = apptNo;
	}
	public String getObjID() {
		return objID;
	}
	public void setObjID(String objID) {
		this.objID = objID;
	}
	public String getResource_URL() {
		return resource_URL;
	}
	public void setResource_URL(String resource_URL) {
		this.resource_URL = resource_URL;
	}
	public String getSalesRep() {
		return salesRep;
	}
	public void setSalesRep(String salesRep) {
		this.salesRep = salesRep;
	}
	public Integer getSP1Split() {
		return SP1Split;
	}
	public void setSP1Split(Integer sP1Split) {
		SP1Split = sP1Split;
	}
	public String getUdfValue() {
		return udfValue;
	}
	public void setUdfValue(String udfValue) {
		this.udfValue = udfValue;
	}
	public String getUDFCode() {
		return UDFCode;
	}
	public void setUDFCode(String uDFCode) {
		UDFCode = uDFCode;
	}
	public String getOrderCategory() {
		return OrderCategory;
	}
	public void setOrderCategory(String orderCategory) {
		OrderCategory = orderCategory;
	}

	@Override
	public String toString() {
		return "--------WebOrderHeaderTableClass-------\nWebOrderHeaderTableClass::POKEY = " + POKEY
				+ "\nWebOrderHeaderTableClass::SessionID = " + SessionID + "\nWebOrderHeaderTableClass::CustomerID = "
				+ CustomerID + "\nWebOrderHeaderTableClass::WebOrderNumber = " + WebOrderNumber
				+ "\nWebOrderHeaderTableClass::WebOrderID = " + WebOrderID
				+ "\nWebOrderHeaderTableClass::SalesOrderId = " + SalesOrderId
				+ "\nWebOrderHeaderTableClass::InvoiceNumber = " + InvoiceNumber
				+ "\nWebOrderHeaderTableClass::OrderDate = " + OrderDate + "\nWebOrderHeaderTableClass::CreatedDate = "
				+ CreatedDate + "\nWebOrderHeaderTableClass::InvoiceDate = " + InvoiceDate
				+ "\nWebOrderHeaderTableClass::OrderStatus = " + OrderStatus
				+ "\nWebOrderHeaderTableClass::Fulfilment_Status = " + Fulfilment_Status
				+ "\nWebOrderHeaderTableClass::FirstName = " + FirstName + "\nWebOrderHeaderTableClass::LastName = "
				+ LastName + "\nWebOrderHeaderTableClass::Salutation = " + Salutation
				+ "\nWebOrderHeaderTableClass::Webstore = " + Webstore + "\nWebOrderHeaderTableClass::OrderType = "
				+ OrderType + "\nWebOrderHeaderTableClass::Currency = " + Currency
				+ "\nWebOrderHeaderTableClass::Vendor = " + Vendor + "\nWebOrderHeaderTableClass::AppId = " + AppId
				+ "\nWebOrderHeaderTableClass::Deptno = " + Deptno + "\nWebOrderHeaderTableClass::EDI_DeptNo = "
				+ EDI_DeptNo + "\nWebOrderHeaderTableClass::ShipTo = " + ShipTo
				+ "\nWebOrderHeaderTableClass::ShipMethod = " + ShipMethod
				+ "\nWebOrderHeaderTableClass::ShipMethodDesc = " + ShipMethodDesc
				+ "\nWebOrderHeaderTableClass::TrackingNumber = " + TrackingNumber
				+ "\nWebOrderHeaderTableClass::ShipmentId = " + ShipmentId
				+ "\nWebOrderHeaderTableClass::ShipmentWeightUOM = " + ShipmentWeightUOM
				+ "\nWebOrderHeaderTableClass::UnitShipped = " + UnitShipped
				+ "\nWebOrderHeaderTableClass::ShipmentWeight = " + ShipmentWeight
				+ "\nWebOrderHeaderTableClass::BOL = " + BOL + "\nWebOrderHeaderTableClass::ShipDate = " + ShipDate
				+ "\nWebOrderHeaderTableClass::ShippingCost = " + ShippingCost
				+ "\nWebOrderHeaderTableClass::EventCode = " + EventCode + "\nWebOrderHeaderTableClass::BlanketNo = "
				+ BlanketNo + "\nWebOrderHeaderTableClass::TermType = " + TermType
				+ "\nWebOrderHeaderTableClass::TermBasisDate = " + TermBasisDate
				+ "\nWebOrderHeaderTableClass::TermsAmt = " + TermsAmt + "\nWebOrderHeaderTableClass::TermsDiscDue = "
				+ TermsDiscDue + "\nWebOrderHeaderTableClass::TermsNetDue = " + TermsNetDue
				+ "\nWebOrderHeaderTableClass::RequestShipDate = " + RequestShipDate
				+ "\nWebOrderHeaderTableClass::ShipNotB4Date = " + ShipNotB4Date
				+ "\nWebOrderHeaderTableClass::CancelAfterDate = " + CancelAfterDate
				+ "\nWebOrderHeaderTableClass::ReqDeliveryDate = " + ReqDeliveryDate
				+ "\nWebOrderHeaderTableClass::InvAlias = " + InvAlias + "\nWebOrderHeaderTableClass::Promo = " + Promo
				+ "\nWebOrderHeaderTableClass::Type = " + Type + "\nWebOrderHeaderTableClass::ImportDate = "
				+ ImportDate + "\nWebOrderHeaderTableClass::BackOrder = " + BackOrder
				+ "\nWebOrderHeaderTableClass::ShipNow = " + ShipNow + "\nWebOrderHeaderTableClass::PrintNow = "
				+ PrintNow + "\nWebOrderHeaderTableClass::ExportNow = " + ExportNow
				+ "\nWebOrderHeaderTableClass::Exported = " + Exported + "\nWebOrderHeaderTableClass::AccExportNow = "
				+ AccExportNow + "\nWebOrderHeaderTableClass::AccExported = " + AccExported
				+ "\nWebOrderHeaderTableClass::ContactName = " + ContactName
				+ "\nWebOrderHeaderTableClass::ContactPhone = " + ContactPhone
				+ "\nWebOrderHeaderTableClass::Cancelled = " + Cancelled + "\nWebOrderHeaderTableClass::ExtraField1 = "
				+ ExtraField1 + "\nWebOrderHeaderTableClass::ExtraField2 = " + ExtraField2
				+ "\nWebOrderHeaderTableClass::ExtraField3 = " + ExtraField3
				+ "\nWebOrderHeaderTableClass::ExtraField4 = " + ExtraField4
				+ "\nWebOrderHeaderTableClass::ExtraField5 = " + ExtraField5
				+ "\nWebOrderHeaderTableClass::ExtraField6 = " + ExtraField6
				+ "\nWebOrderHeaderTableClass::ExtraField7 = " + ExtraField7
				+ "\nWebOrderHeaderTableClass::ExtraField8 = " + ExtraField8
				+ "\nWebOrderHeaderTableClass::ExtraField9 = " + ExtraField9
				+ "\nWebOrderHeaderTableClass::ExtraField10 = " + ExtraField10
				+ "\nWebOrderHeaderTableClass::ArchiveNow = " + ArchiveNow + "\nWebOrderHeaderTableClass::Confirmed = "
				+ Confirmed + "\nWebOrderHeaderTableClass::ChangesExported = " + ChangesExported
				+ "\nWebOrderHeaderTableClass::PriceList = " + PriceList + "\nWebOrderHeaderTableClass::ExportDate = "
				+ ExportDate + "\nWebOrderHeaderTableClass::AckSentStatus = " + AckSentStatus
				+ "\nWebOrderHeaderTableClass::AckSentDate = " + AckSentDate
				+ "\nWebOrderHeaderTableClass::EmailAddress = " + EmailAddress
				+ "\nWebOrderHeaderTableClass::IPAddress = " + IPAddress + "\nWebOrderHeaderTableClass::OrderNotes = "
				+ OrderNotes + "\nWebOrderHeaderTableClass::GiftNotes = " + GiftNotes
				+ "\nWebOrderHeaderTableClass::Subtotal = " + Subtotal + "\nWebOrderHeaderTableClass::Discount = "
				+ Discount + "\nWebOrderHeaderTableClass::Tax = " + Tax + "\nWebOrderHeaderTableClass::Tax2 = " + Tax2
				+ "\nWebOrderHeaderTableClass::TaxRate = " + TaxRate + "\nWebOrderHeaderTableClass::TaxRate2 = "
				+ TaxRate2 + "\nWebOrderHeaderTableClass::TaxCode = " + TaxCode
				+ "\nWebOrderHeaderTableClass::TaxCode2 = " + TaxCode2 + "\nWebOrderHeaderTableClass::GrandTotal = "
				+ GrandTotal + "\nWebOrderHeaderTableClass::Residential = " + Residential
				+ "\nWebOrderHeaderTableClass::Source = " + Source + "\nWebOrderHeaderTableClass::ConfigID = "
				+ ConfigID + "\nWebOrderHeaderTableClass::Warehouse_location = " + Warehouse_location
				+ "\nWebOrderHeaderTableClass::Credit_card_type = " + Credit_card_type
				+ "\nWebOrderHeaderTableClass::Credit_card_name = " + Credit_card_name
				+ "\nWebOrderHeaderTableClass::Credit_card_number = " + Credit_card_number
				+ "\nWebOrderHeaderTableClass::Credit_card_expiry = " + Credit_card_expiry
				+ "\nWebOrderHeaderTableClass::Credit_card_code = " + Credit_card_code
				+ "\nWebOrderHeaderTableClass::IsShipmentOrder = " + IsShipmentOrder
				+ "\nWebOrderHeaderTableClass::Conversation = " + Conversation
				+ "\nWebOrderHeaderTableClass::Payment_status = " + Payment_status
				+ "\nWebOrderHeaderTableClass::updatedDate = " + updatedDate + "\nWebOrderHeaderTableClass::FOB = "
				+ FOB + "\nWebOrderHeaderTableClass::Ultrec = " + Ultrec
				+ "\nWebOrderHeaderTableClass::PercentageDiscount = " + PercentageDiscount
				+ "\nWebOrderHeaderTableClass::CouponCode = " + CouponCode + "\nWebOrderHeaderTableClass::EDI_FOB = "
				+ EDI_FOB + "\nWebOrderHeaderTableClass::ProductType = " + ProductType
				+ "\nWebOrderHeaderTableClass::InternalControlNumber = " + InternalControlNumber
				+ "\nWebOrderHeaderTableClass::Payee_identification = " + Payee_identification
				+ "\nWebOrderHeaderTableClass::customer_order_number = " + customer_order_number
				+ "\nWebOrderHeaderTableClass::Subday_number = " + Subday_number
				+ "\nWebOrderHeaderTableClass::TermsDescription = " + TermsDescription
				+ "\nWebOrderHeaderTableClass::Sender_Qual = " + Sender_Qual
				+ "\nWebOrderHeaderTableClass::Sender_ID = " + Sender_ID + "\nWebOrderHeaderTableClass::Sender_GS = "
				+ Sender_GS + "\nWebOrderHeaderTableClass::EDIDesktop_Retailer = " + EDIDesktop_Retailer
				+ "\nWebOrderHeaderTableClass::ProNumber = " + ProNumber
				+ "\nWebOrderHeaderTableClass::ShippingNotes = " + ShippingNotes
				+ "\nWebOrderHeaderTableClass::Three3PLOrderID = " + Three3PLOrderID
				+ "\nWebOrderHeaderTableClass::ShippedCost = " + ShippedCost + "\nWebOrderHeaderTableClass::ApptNo = "
				+ ApptNo + "\nWebOrderHeaderTableClass::objID = " + objID
				+ "\nWebOrderHeaderTableClass::resource_URL = " + resource_URL
				+ "\nWebOrderHeaderTableClass::salesRep = " + salesRep + "\nWebOrderHeaderTableClass::SP1Split = "
				+ SP1Split + "\nWebOrderHeaderTableClass::udfValue = " + udfValue
				+ "\nWebOrderHeaderTableClass::UDFCode = " + UDFCode + "\nWebOrderHeaderTableClass::OrderCategory = "
				+ OrderCategory + "\nWebOrderHeaderTableClass::flatFields = " + Arrays.toString(flatFields)
				+ "\nWebOrderHeaderTableClass::nestedFields = " + Arrays.toString(nestedFields)
				+ "\nWebOrderHeaderTableClass::attributes = " + Arrays.toString(attributes)
				+ "\nWebOrderHeaderTableClass";
	}

}
