package sqlServerToAmazonDB.getSessionOrders;

import java.util.Arrays;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "WebServiceSession")
public class WebServiceSession implements XMLItem {

	
	@Column (name = "LegacySessionID")
	private Integer SessionID;
	
	@Id
	@Column (name = "SessionID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer NewSessionID;
	
	
	private int SourceID;
	private String HostName;
	private String WorkFlowName;
	private String BatchID;
	private String CustomerID;
	private String JsonData;
	private String StartDate;
	private String EndDate;
	private int MapFailed;
	private String Description;
	private int NbSuccess;
	private int NbFailed;
	private String Note;
	private String BatchSize;
	private String NbDocument;
	private String NbTransaction;
	private String NbSucceed;
	private String Conversation;
	private String SessionToken;
	private String SourceContentSize;
	private String TargetContentSize;
	private String ResponseContentSize;
	private String SessionLogsContentSize;

	
	@ElementCollection(targetClass = WebOrderHeader.class)
	@CollectionTable(name = "WebOrderHeader", joinColumns = @JoinColumn(name = "SessionID"))
	private List<XMLItem> webOrderHeader;
	
	
	@ElementCollection(targetClass = WebServiceDTS.class)
	@CollectionTable(name = "WebServiceDTS", joinColumns = @JoinColumn(name = "SessionID"))
	private List<XMLItem> webServiceDTS;

	@ElementCollection(targetClass = WebOrderAddresses.class)
	@CollectionTable(name = "WebOrderAddresses", joinColumns = @JoinColumn(name = "SessionID"))
	private List<XMLItem> webOrderAddresses;

	
	@ElementCollection(targetClass = WebOrderDetail.class)
	@CollectionTable(name = "WebOrderDetail", joinColumns = @JoinColumn(name = "SessionID"))
	private List<XMLItem> webOrderDetail;

	@Transient
	private String[] flatFields = { "SessionID", "SourceID", "HostName", "WorkFlowName", "BatchID", "CustomerID",
			"JsonData", "StartDate", "EndDate", "MapFailed", "Description", "NbSuccess", "NbFailed", "Note",
			"BatchSize", "NbDocument", "NbTransaction", "NbSucceed", "Conversation", "sessionToken",
			"sourceContentSize", "targetContentSize", "responseContentSize", "sessionLogsContentSize" };

	@Transient
	private String[] nestedFields = { "WebServiceDTS", "WebOrderHeader", "WebOrderAddresses",
			"WebOrderDetail" };

	@Transient
	private String[] attributes = { "SessionID", "SourceID", "HostName", "WorkFlowName", "BatchID", "CustomerID",
			"JsonData", "StartDate", "EndDate", "MapFailed", "Description", "NbSuccess", "NbFailed", "Note",
			"BatchSize", "NbDocument", "NbTransaction", "NbSucceed", "Conversation", "sessionToken",
			"sourceContentSize", "targetContentSize", "responseContentSize", "sessionLogsContentSize" };
	// "WebServiceDTS"};

	public Integer getSessionID() {
		return SessionID;
	}

	public void setSessionID(Integer sessionID) {
		SessionID = sessionID;
	}

	public String getWorkFlowName() {
		return WorkFlowName;
	}

	public void setWorkFlowName(String workFlowName) {
		WorkFlowName = workFlowName;
	}

	public String getCustomerID() {
		return CustomerID;
	}

	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}

	public String getJsonData() {
		return JsonData;
	}

	public void setJsonData(String jsonData) {
		JsonData = jsonData;
	}

	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public String getEndDate() {
		return EndDate;
	}

	public void setEndDate(String endDate) {
		EndDate = endDate;
	}

	public Integer getMapFailed() {
		return MapFailed;
	}

	public void setMapFailed(Integer mapFailed) {
		MapFailed = mapFailed;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public Integer getNbSuccess() {
		return NbSuccess;
	}

	public void setNbSuccess(Integer nbSuccess) {
		NbSuccess = nbSuccess;
	}

	public Integer getNbFailed() {
		return NbFailed;
	}

	public void setNbFailed(Integer nbFailed) {
		NbFailed = nbFailed;
	}

	public List<XMLItem> getWebOrderAddresses() {
		return webOrderAddresses;
	}

	public void setWebOrderAddresses(List<XMLItem> webOrderAddresses) {
		this.webOrderAddresses = webOrderAddresses;
	}
	public String getNote() {
		return Note;
	}

	public void setNote(String note) {
		Note = note;
	}

	public String getBatchSize() {
		return BatchSize;
	}

	public void setBatchSize(String batchSize) {
		BatchSize = batchSize;
	}

	public void setSourceID(Integer sourceID) {
		this.SourceID = sourceID;
	}

	public int getSourceID() {
		return SourceID;
	}

	public String getHostName() {
		return HostName;
	}

	public void setHostName(String hostName) {
		HostName = hostName;
	}

	public String getBatchID() {
		return BatchID;
	}

	public void setBatchID(String batchID) {
		BatchID = batchID;
	}

	public String getNbDocument() {
		return NbDocument;
	}

	public void setNbDocument(String nbDocument) {
		NbDocument = nbDocument;
	}

	public String getNbTransaction() {
		return NbTransaction;
	}

	public void setNbTransaction(String nbTransaction) {
		NbTransaction = nbTransaction;
	}

	public String getNbSucceed() {
		return NbSucceed;
	}

	public void setNbSucceed(String nbSucceed) {
		NbSucceed = nbSucceed;
	}

	public String getConversation() {
		return Conversation;
	}

	public void setConversation(String conversation) {
		Conversation = conversation;
	}

	public String getSessionToken() {
		return SessionToken;
	}

	public void setSessionToken(String sessionToken) {
		SessionToken = sessionToken;
	}

	public String getSourceContentSize() {
		return SourceContentSize;
	}

	public void setSourceContentSize(String sourceContentSize) {
		SourceContentSize = sourceContentSize;
	}

	public String getTargetContentSize() {
		return TargetContentSize;
	}

	public void setTargetContentSize(String targetContentSize) {
		TargetContentSize = targetContentSize;
	}

	public String getResponseContentSize() {
		return ResponseContentSize;
	}

	public void setResponseContentSize(String responseContentSize) {
		ResponseContentSize = responseContentSize;
	}

	public String getSessionLogsContentSize() {
		return SessionLogsContentSize;
	}

	public void setSessionLogsContentSize(String sessionLogsContentSize) {
		SessionLogsContentSize = sessionLogsContentSize;
	}

	public List<XMLItem> getWebOrderHeader() {
		return webOrderHeader;
	}

	public List<XMLItem> getWebServiceDTS() {
		return webServiceDTS;
	}

	public void setWebServiceDTS(List<XMLItem> webServiceDTS) {
		this.webServiceDTS = webServiceDTS;
	}


	public List<XMLItem> getWebOrderDetail() {
		return webOrderDetail;
	}

	public void setWebOrderDetail(List<XMLItem> webOrderDetail) {
		this.webOrderDetail = webOrderDetail;
	}

	@Override
	public String[] getFlatFields() {
		// TODO Auto-generated method stub
		return flatFields;
	}

	@Override
	public String[] getNestedFields() {
		// TODO Auto-generated method stub
		return nestedFields;
	}

	@Override
	public String[] getAttributes() {
		// TODO Auto-generated method stub
		return attributes;
	}

	public void setWebOrderHeader(List<XMLItem> items) {
		// TODO Auto-generated method stub
		this.webOrderHeader = items;
	}

	@Override
	public String toString() {
		return "--------WebServiceSessionTableClass-------\nWebServiceSessionTableClass::SessionID = " + SessionID
				+ "\nWebServiceSessionTableClass::SourceID = " + SourceID + "\nWebServiceSessionTableClass::HostName = "
				+ HostName + "\nWebServiceSessionTableClass::WorkFlowName = " + WorkFlowName
				+ "\nWebServiceSessionTableClass::BatchID = " + BatchID + "\nWebServiceSessionTableClass::CustomerID = "
				+ CustomerID + "\nWebServiceSessionTableClass::JsonData = " + JsonData
				+ "\nWebServiceSessionTableClass::StartDate = " + StartDate
				+ "\nWebServiceSessionTableClass::EndDate = " + EndDate + "\nWebServiceSessionTableClass::MapFailed = "
				+ MapFailed + "\nWebServiceSessionTableClass::Description = " + Description
				+ "\nWebServiceSessionTableClass::NbSuccess = " + NbSuccess
				+ "\nWebServiceSessionTableClass::NbFailed = " + NbFailed + "\nWebServiceSessionTableClass::Note = "
				+ Note + "\nWebServiceSessionTableClass::BatchSize = " + BatchSize
				+ "\nWebServiceSessionTableClass::NbDocument = " + NbDocument
				+ "\nWebServiceSessionTableClass::NbTransaction = " + NbTransaction
				+ "\nWebServiceSessionTableClass::NbSucceed = " + NbSucceed
				+ "\nWebServiceSessionTableClass::Conversation = " + Conversation
				+ "\nWebServiceSessionTableClass::SessionToken = " + SessionToken
				+ "\nWebServiceSessionTableClass::SourceContentSize = " + SourceContentSize
				+ "\nWebServiceSessionTableClass::TargetContentSize = " + TargetContentSize
				+ "\nWebServiceSessionTableClass::ResponseContentSize = " + ResponseContentSize
				+ "\nWebServiceSessionTableClass::SessionLogsContentSize = " + SessionLogsContentSize
				+ "\nWebServiceSessionTableClass::flatFields = " + Arrays.toString(flatFields)
				+ "\nWebServiceSessionTableClass::nestedFields = " + Arrays.toString(nestedFields)
				+ "\nWebServiceSessionTableClass::attributes = " + Arrays.toString(attributes)
				+ "\nWebServiceSessionTableClass::WebOrderDTS = " + webServiceDTS.toString()
				+ "\nWebServiceSessionTableClass";
	}
}
