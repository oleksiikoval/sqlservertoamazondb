package sqlServerToAmazonDB.MainApp;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import sqlServerToAmazonDB.getSessionOrders.*;

public class MyHandler0 extends DefaultHandler {

	int counter = 0;
	// Variables for handler
	private String tag = "";
	// Reference and instance of dynamic class
	private Class<?> reference;
	private Object instance;
	

	// Array to hold class methods, and HashMap to match fields to them
	private Method[] methods = {}; // stores setters for the dynamic instances
	private HashMap<String, Integer> map; // we map fields with the method index
											// from the array

	// Arrays to hold information about XML fields
	private String[] fields; // all the flat fields
	private String[] nested;
	private String[] attributeFields;

	// StringBuilder to accumulate characters
	private StringBuilder stringBuilder;
	private StringBuilder nestedStringBuilder;

	// String to hold nested element names
	List<String> elementName = new ArrayList<String>();
	String nestedItemName = "";

	// List to hold XMLItems
	private List<XMLItem> itemList;
	private XMLItem container;

	// setting up container for nested items
	private XMLItem nestedContainer = null;

	// Boolean flags for regulating flow
	private boolean flatMatch = false;
	private boolean nestedMatch = false;
	private boolean attributeMatch = false;
	private boolean insideNested = false;

	private String inputXmlString;
	
	public MyHandler0(String tag, String inputXmlString, XMLItem container) throws IOException {
		// Initialize globals
		itemList = new ArrayList<XMLItem>();
		stringBuilder = new StringBuilder();
		nestedStringBuilder = new StringBuilder();
		
		this.inputXmlString=inputXmlString;
		// Assign values from container parameters
		this.tag = tag;
		this.container = container;
		fields = container.getFlatFields();
		nested = container.getNestedFields();
		attributeFields = container.getAttributes();
	}

	public List<XMLItem> getItems() {
		return itemList;
	}

	public XMLItem getItem() {
		return itemList.get(0);
	}

	/**
	 * more generic method to get XMLItems for the selected index
	 * 
	 * @return XMLItem at given index
	 */

	public XMLItem getItem(int i) {
		return itemList.get(i);
	}

	public boolean checkNumberParsed() {
		return counter == itemList.size();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		// start for the root tag or any top level tag ....
		try {
			if (qName.equalsIgnoreCase(tag)) { // match found for the root tag
												// or any top level tag
				// tasks at hand now
				// 1 check the content of the tag which can be nested tags or
				// simple string
				// 2 check if any attributes?
				// 3 get a list of nested elements
				// 4 gather info for the attributes
				// 5 in endElement() set the next element to tag and qName
				// values

				// ------------------task 1 initial
				// setup----------------------------
				// must run one time only

				if (itemList.isEmpty()) {// create a list to hold the nested
											// elements if any
					itemList = new ArrayList<XMLItem>();
					map = new HashMap<String, Integer>();

					// create the reference object for reflecting the methods
					// from the container
					reference = Class.forName(container.getClass().getName());
					methods = reference.getMethods();// all the methods from the
														// container class

					// now we need to map all the setters with appropriate
					// fields from the reference class into a map with field
					// names and index of the method array
					int i = 0; // counter to match the index of the method

					for (Method method : methods) { // iterate through all the
													// methods to match its
													// fields...
						// check all the flat fields first...
						flatMatch = false;
						for (String item : fields) {// iterate through flat
													// fields
							if (method.getName().startsWith("set")) { // take
																		// only
																		// setters
																		// into
																		// consideration
								// find the setter for the current field only
								if (("set" + item.replaceAll("_|-", "")).equalsIgnoreCase(method.getName())) {
									flatMatch = true; // pair matched
									////System.out.println("Item-Method match found------" + item);
									map.put(item, i); // add the item with
														// methods[] index into
														// the map
								}
							}

							// check if the matching pair is already set into
							// the map? and break the inner loop to check for
							// the next item

							if (flatMatch) {
								break;
							}
						}

						// now go for the nested fields if any
						for (String item : nested) {
							nestedMatch = false;
							if (method.getName().startsWith("set")) { // take
																		// only
																		// setters
																		// into
																		// consideration
								// find the setter for the current field only
								if (("set" + item.replaceAll("_|-", "")).equalsIgnoreCase(method.getName())) {
									nestedMatch = true; // pair matched
									////System.out.println("Nested-Item-Method match found------" + item);
									map.put(item, i); // add the item with
														// methods[] index into
														// the map

									// find the methods from nested container to
									// set attributes of nested items...

								}
							}
							if (nestedMatch) {
								break;
							}
						}

						// now go for attributes if any
						for (String item : attributeFields) {
							attributeMatch = false;
							if (method.getName().startsWith("set")) { // take
																		// only
																		// setters
																		// into
																		// consideration
								// find the setter for the current field only
								if (("set" + item.replaceAll("_|-", "")).equalsIgnoreCase(method.getName())) {
									attributeMatch = true; // pair matched
									////System.out.println("Attribute-Item-Method match found------" + item);
									map.put(item, i); // add the item with
														// methods[] index into
														// the map
								}
							}
							if (attributeMatch) {
								break;
							}
						}
						i++; // increase the methods[] index for matching next
								// method
					}
			////		System.out.println("-----------------Match making finished---------------------");
				////	System.out.println("----printing map-----" + map.toString());
				}

				if (instance == null) {// if the instance of current
										// container is null then create a
										// new instance on which
										// all the setters will be invoked
										// using reflection
					instance = reference.newInstance();// now reference instance
														// is created so all the
														// matching setters can
														// be invoked
					
				}
			}
			
			
			

			// ----------start gathering values from the xml document for each
			// flat filed only------------------------
			if (!insideNested) { // make sure flat fields code is not executed
									// again for the nested fields
				for (String item : fields) {
					flatMatch = false;
					if (qName.equalsIgnoreCase(item)) {// match found for the
														// tag name with a flat
														// field
						flatMatch = true;// setting a marker if a flat field is
											// found
											// then proceed to check if any
											// attributes are declared for that
											// field?

						// getting the string data from the flat elements...
						// code to retrieve string data from the flat field

					}
					if (flatMatch) {
						break;
					}
				}
			}

			// -------------gathering all the attributes for the marked flat
			// field---------
			
			System.out.println("inside start:"+attributes.getLength());
			
			getAttributesOfElement(attributes);
			
			// ------------gathering string values from the nested fields
			// --------
			getNestedFieldValues(qName, attributes);
			
		} catch (ClassNotFoundException e) {

			System.out.println("Class Missing" + e.getCause());
		} catch (InstantiationException e) {
			System.out.println("reference instantiation failed" + e.getCause());
		} catch (IllegalAccessException e) {
			System.out.println("reference accessed illigally" + e.getCause());
		}
	}
	/*
	 * @Override public void endElement(String uri, String localName, String
	 * qName) throws SAXException { //--------start calling all the setters for
	 * previously recognized elements --------- try{ //----start invoking
	 * setters for the current container----- if (flatMatch && !insideNested) {
	 * // Setup the setter method from the container class Method method =
	 * reference.getDeclaredMethod((methods[map.get(qName)].getName()),
	 * String.class); // Invoked the setter method from the container class
	 * method.invoke(instance, stringBuilder.toString().trim()); stringBuilder =
	 * new StringBuilder(); flatMatch = false; nestedMatch = false; } else if
	 * (insideNested && elementName.contains(qName)) { String closingTag = "</"
	 * + qName + ">"; nestedStringBuilder.append(closingTag); flatMatch = false;
	 * nestedMatch = false; } else if (qName.equalsIgnoreCase(nestedItemName) &&
	 * insideNested) { String closingTag = "</" + qName + ">";
	 * nestedStringBuilder.append(closingTag); // Create InputSource for the SAX
	 * Parser InputSource input = new InputSource( new
	 * StringReader(nestedStringBuilder.toString().trim().replaceAll("&",
	 * "&amp;")));
	 * 
	 * // Create a SAX Parser to run over the nested item SAXParserFactory
	 * parserFactory = SAXParserFactory.newInstance(); SAXParser parser =
	 * parserFactory.newSAXParser();
	 * 
	 * // Get the ClassName and TagName for the nested object String nestedTag =
	 * qName; String className = qName.replaceAll("-|_", " ");
	 * 
	 * // Replace special characters and capitalize to mimic casing of // class
	 * names className = WordUtils.capitalizeFully(className);// Capitalize //
	 * to // emulate // class // naming className = className.replaceAll(" ",
	 * "");// Remove the spaces
	 * 
	 * // Setup the class for our nested item String containerName =
	 * container.getClass().getName(); String simpleName =
	 * container.getClass().getSimpleName(); Class<?> nestedClass = Class
	 * .forName(containerName.substring(0, containerName.length() -
	 * simpleName.length()) + className);
	 * 
	 * // Create a handler object to manage parsing events NewMyHandler handler
	 * = new NewMyHandler(nestedTag, (XMLItem) nestedClass.newInstance());
	 * 
	 * // Parse the document parser.parse(input, handler);
	 * 
	 * // Get parameters for the method Class<?>[] parameters =
	 * methods[map.get(qName)].getParameterTypes();
	 * 
	 * // Get the method if (parameters[0] == List.class) { Method method =
	 * reference.getDeclaredMethod(methods[map.get(qName)].getName(),
	 * parameters[0]); method.invoke(instance, handler.getItems()); } else {
	 * Method method =
	 * reference.getDeclaredMethod(methods[map.get(qName)].getName(),
	 * parameters[0]); method.invoke(instance, handler.getItem()); }
	 * insideNested = false; elementName.clear(); nestedStringBuilder = new
	 * StringBuilder(); } else if (qName.equals(tag)) { counter++; // Add the
	 * existing item itemList.add((XMLItem) instance); // Create a new instance
	 * instance = reference.newInstance(); flatMatch = false; nestedMatch =
	 * false; } } catch (NoSuchMethodException e) { // TODO Auto-generated catch
	 * block e.printStackTrace(); } catch (SecurityException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } catch
	 * (IllegalAccessException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (IllegalArgumentException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } catch
	 * (InvocationTargetException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (ClassNotFoundException e) {
	 * e.printStackTrace(); } catch (IOException e) { // TODO Auto-generated
	 * catch block e.printStackTrace(); } catch (InstantiationException e) { //
	 * TODO Auto-generated catch block e.printStackTrace(); } catch
	 * (ParserConfigurationException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (NullPointerException e) {
	 * System.out.println("Problem parsing " + qName + " in " +
	 * container.getClass().getName()); e.printStackTrace(); insideNested =
	 * false; nestedStringBuilder = new StringBuilder(); } catch
	 * (SAXParseException e) { System.out.println("Problem parsing " + qName +
	 * " in " + nestedStringBuilder.toString().trim()); e.printStackTrace();
	 * insideNested = false; nestedStringBuilder = new StringBuilder(); }
	 * 
	 * }
	 */

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		// ------------------------------------------------------------------------------------------------------------------\\
		// ------------------------------------------FOR CALLING
		// SETTERS-----------------------------------------------------\\
		// ------------------------------------------------------------------------------------------------------------------\\
		
		try {
			// Just invoke the appropriate setter for the current container

			if (flatMatch && !insideNested) {
				System.out.println("1");
				// Setup the setter method from the container class
				Method method = reference.getDeclaredMethod((methods[map.get(qName)].getName()), String.class);
				// Invoked the setter method from the container class
				method.invoke(instance, stringBuilder.toString().trim());
				stringBuilder = new StringBuilder();
				flatMatch = false;
				nestedMatch = false;
				
			}

			else if (qName.equalsIgnoreCase(nestedItemName) && insideNested) {
				System.out.println("end case 3");
				String closingTag = "</" + qName + ">";
				nestedStringBuilder.append(closingTag);
				// Create InputSource for the SAX Parser
				System.out.println("mingquan3 " + nestedStringBuilder.toString());
				
				
				// InputSource input = new InputSource(new
				// StringReader(nestedStringBuilder.toString().trim().replaceAll("&",
				// "&amp;")));

				// StreamConverter sc= new StreamConverter();
				// System.out.println(sc.getString(input.getByteStream()));
				// Create a SAX Parser to run over the nested item
				SAXParserFactory parserFactory = SAXParserFactory.newInstance();
				SAXParser parser = parserFactory.newSAXParser();

				// Get the ClassName and TagName for the nested object
				String nestedTag = qName;
				String className = qName.replaceAll("-|_", " ");
				// System.out.println("---------" + className);

				/*
				 * if(qName.endsWith("lines")||qName.endsWith("fields")||qName.
				 * endsWith("items")||qName.endsWith("names")||qName.endsWith(
				 * "attributes")||qName.endsWith("codes")||qName.endsWith(
				 * "refunds")||qName.endsWith("transactions")||qName.endsWith(
				 * "adjustments")){ nestedTag = nestedTag.substring(0,
				 * qName.length()-1); className = className.substring(0,
				 * qName.length()-1); } else if(qName.endsWith("taxes")){
				 * nestedTag = nestedTag.substring(0, qName.length()-2);
				 * className = className.substring(0, qName.length()-2); }
				 */
				nestedTag = nestedTag.substring(0, qName.length());
				className = className.substring(0, qName.length());

				// Replace special characters and capitalize to mimic casing of
				// class names
				System.out.println(className);
				// className = WordUtils.capitalizeFully(className);//Capitalize
				// to emulate class naming
				className = className.replaceAll(" ", "");// Remove the spaces

				// Setup the class for our nested item
				String containerName = container.getClass().getName();
				String simpleName = container.getClass().getSimpleName();
				// System.out.println(containerName);
				// System.out.println(simpleName);
				// System.out.println(className);
				Class<?> nestedClass = Class
						.forName(containerName.substring(0, containerName.length() - simpleName.length()) + className);
				System.out.println("*******" + nestedClass);
				// Create a handler object to manage parsing events
				// System.out.println("nestedTag:" + nestedTag);
				// System.out.println(nestedClass.newInstance());

				
				
				int starti = inputXmlString.indexOf(nestedTag);
	    		int endj = inputXmlString.indexOf("/"+nestedTag);

	    		String temp = inputXmlString.substring(0,starti-1);
	    		String temp2 = inputXmlString.substring(endj);
				//inputXmlString=temp+temp2;
				
				
				
				
	    		MyHandler0 handler = new MyHandler0(nestedTag,inputXmlString, (XMLItem) nestedClass.newInstance());
				InputSource input = new InputSource(
						new StringReader(nestedStringBuilder.toString().trim().replaceAll("&", "&amp;")+closingTag));
				
				
				// Parse the document
				// parser.parse(input, handler);
				parser.parse(new InputSource(new StringReader(this.inputXmlString)), handler);
				// Get parameters for the method
				Class<?>[] parameters = methods[map.get(qName)].getParameterTypes();

				// Get the method
				if (parameters[0] == List.class) {
					Method method = reference.getDeclaredMethod(methods[map.get(qName)].getName(), parameters[0]);
					method.invoke(instance, handler.getItems());
				} else {
					Method method = reference.getDeclaredMethod(methods[map.get(qName)].getName(), parameters[0]);
					method.invoke(instance, handler.getItem());
				}
				insideNested = false;
				elementName.clear();
				nestedStringBuilder = new StringBuilder();
				
			} else if (qName.equals(tag)) {
				System.out.println("Finished " + tag + " tag block parsing.");
				counter++;
				
					

				if(instance != null ){
				// Add the existing item
					Class<?> class2 = instance.getClass();
					java.lang.reflect.Field field = class2.getDeclaredFields()[0];
						field.setAccessible(true);
						
						//checking if the first element of instance is not empty or 0, so the extra empty object doesn't get added
						if(field.get(instance) != null && field.get(instance) != ""){
							if (field.getType() == Integer.class) {
								if (Integer.parseInt(field.get(instance).toString()) != 0) {
									System.out.println("~"+field.get(instance));
									System.out.println("$"+"counter==="+counter);
									itemList.add((XMLItem) instance);
								} 
							} else {
								itemList.add((XMLItem) instance);
							}
							
							
						}
					
					
					
				}
				// Create a new instance
				
				instance = reference.newInstance();
				flatMatch = false;
				nestedMatch = false;
			}
			
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			System.out.println("Problem parsing " + qName + " in " + container.getClass().getName());
			e.printStackTrace();
			insideNested = false;
			nestedStringBuilder = new StringBuilder();
		} catch (SAXParseException e) {
			System.out.println("Problem parsing " + qName + " in " + nestedStringBuilder.toString().trim());
			e.printStackTrace();
			insideNested = false;
			nestedStringBuilder = new StringBuilder();
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		// SAX doesn't always serve up all characters in one character call, so
		// we append to a StringBuilder to ensure we get everything
		if (flatMatch) {
			stringBuilder.append(new String(ch, start, length));
		}
		// If the name is set to something, we should grab it
		if (insideNested) {
			nestedStringBuilder.append(new String(ch, start, length));
		}
	}

	public void getAttributesOfElement(Attributes attributes) {
		if (attributeFields != null && attributeFields.length > 0 && map != null) {
			for (String item : attributeFields) {// iterate through all the
													// attributes and call
													// respective setters

				try {
				/*	//System.out.println("% after 20 ");
					if(attributes.getLength() >0){
						System.out.println("% "+attributes.getValue(item).toString());
						
					}*/
					// find the associated method for the item from the map
					if (map.get(item) != null) {
						// fetch parameters for the method if any
						Class<?>[] parameters = methods[map.get(item)].getParameterTypes();
						// find the setter method from the container class
						Method method = reference.getDeclaredMethod(methods[map.get(item)].getName(), parameters[0]);
						// now invoke the setter method
						// check parameter type
						if (attributes.getValue(item) != null) {
							if (parameters[0] == String.class) {
								method.invoke(instance, String.valueOf(attributes.getValue(item)));
							} else if (parameters[0] == Integer.class) {
								method.invoke(instance, Integer.valueOf(attributes.getValue(item)));
							} else if (parameters[0] == Double.class) {
								method.invoke(instance, Double.valueOf(attributes.getValue(item)));
							} else if (parameters[0] == Float.class) {
								method.invoke(instance, Float.valueOf(attributes.getValue(item)));
							}
						}
					}
				} catch (NullPointerException e) {
					System.out.println(item + " does not exist inside this element.");
				} catch (NoSuchMethodException e) {
					System.out.println("No such method for " + container.getClass().getName() + "." + item);
				} catch (NumberFormatException e) {
					System.out.println("Failed to format number, possible null?");
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			//attributes = null;
		}else{
			System.out.println("attributes empty");
		}
	}

	public void getNestedFieldValues(String qName, Attributes attributes) {
		//while (!nestedItemName.equals(qName)) {

			if (insideNested) {
				elementName.add(qName);
				String startTag = "<" + qName + ">";
				nestedStringBuilder.append(startTag);
			}

			if (!flatMatch && !insideNested) {// Make sure this doesn't run for
				// any of the flat fields, or
				// things will get very
				// confusing
				for (String field : nested) {
					nestedMatch = false;
				//	getAttributesOfElement(attributes);
					if (qName.equalsIgnoreCase(field)) {
						nestedMatch = true;
					}
					if (nestedMatch) {// We've already found a match, so we
						// break off
						insideNested = true;
						nestedItemName = qName;
						
						String startTag = "<" + nestedItemName + ">";
						nestedStringBuilder.append(startTag);
						break;
					}
				}
			}
		//}
	}

}
