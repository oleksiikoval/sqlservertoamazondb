package sqlServerToAmazonDB.MainApp;

 

import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import sqlServerToAmazonDB.getSessionOrders.WebOrderAddresses;
import sqlServerToAmazonDB.getSessionOrders.WebOrderDetail;
import sqlServerToAmazonDB.getSessionOrders.WebOrderHeader;
import sqlServerToAmazonDB.getSessionOrders.WebServiceDTS;
import sqlServerToAmazonDB.getSessionOrders.WebServiceSession;
import sqlServerToAmazonDB.getSessionOrders.XMLItem;


/**
 * Public class to Insert values to the Database. This version is directed at internal database transfers.
 * @author Ming Quan, modified by Connor Sortome 2016-09-13
 *
 */
public class Insert2Db{

	private Connection conn;
	private SimpleDateFormat dateFormat;

	public Insert2Db() {
		super();
	}

	public Insert2Db(String address, String username, String password){
		try {
			String driver = "com.mysql.jdbc.Driver";
			String dbUsername = "sqladmin";
			String dbPassword = "Virt2ual!";

			Class.forName(driver).newInstance();

			String jdbcUrl = address + "?user=" + dbUsername + "&password=" + dbPassword;

			conn = DriverManager.getConnection(jdbcUrl);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//Set date for this transaction
		dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss z");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Canada/Eastern"));
	}

	public void insertEntries(List<XMLItem> itemList, RestfulInfo restfulInfo){
		//Loop through each SessionTableClass in the list, loop through every sub table, and add them to the database
		for(int i = 0; i < itemList.size(); i++){
			//For WebServiceSessionTable
			WebServiceSession sessionTable = (WebServiceSession)itemList.get(i);

	
			Configuration configuration = new Configuration();
			configuration.configure();
			ServiceRegistry sr = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
					.buildServiceRegistry();
			SessionFactory sf = configuration.buildSessionFactory(sr);
			Session ss = sf.openSession();	
			
			
			
			ss.beginTransaction();
			
			WebOrderAddresses test = (WebOrderAddresses)sessionTable.getWebOrderAddresses().get(i);
			
			
			try{
				
				ss.save(sessionTable);
				
				ss.getTransaction().commit();
			
			} catch(Exception e){
				//TODO Handling
				e.printStackTrace();
			} finally {
				ss.close();
			}
			
			System.out.println("Session is is " + test.getSessionID());
		}
	}

	/**
	 * Public method to convert Java Date to SQL Date. Takes in Java date as a parameter, and returns it as a new SQL Date.
	 * @param date The Java.Util Date to convert
	 * @return The converted Date
	 */
	public static java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
		if(date!=null)
			return new java.sql.Date(date.getTime());
		else
			return null;
	}
}
