package sqlServerToAmazonDB.MainApp;

//This Program is to 

//1. Parse NestedXml;
//2. Store Data in a class "Order"(include other class such as Customer etc)
//3. Insert to tables, and record the lambda function name and Log-stream name in a table called Lambda

//It has two version, one is for testing, using main function, and the restfulinfo value need to set
//another version is Lambda Version, the pass parameters is restfulinfo

//when using in web api, the interface is javascript --string (json format)---> web server javabean  
//(transform by restinfo from string(json) to restfulinfo object, we use restfulinfo in the code, only 
//because it's easier to use set and get method to get all the value in the nested order
//insertDb has branch to deal with three tables, this is much better, because it is clear and easier to change
//this is the second lambda function in the lambda chain link

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLUnit;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;



//import com.thoughtworks.xstream.XStream;
//import com.thoughtworks.xstream.io.xml.StaxDriver;

import sqlServerToAmazonDB.getSessionOrders.XMLItem;
import sqlServerToAmazonDB.getSessionOrders.WebOrderAddresses;

import sqlServerToAmazonDB.getSessionOrders.WebOrderDetail;

import sqlServerToAmazonDB.getSessionOrders.WebOrderHeader;

import sqlServerToAmazonDB.getSessionOrders.WebServiceDTS;

import sqlServerToAmazonDB.getSessionOrders.WebServiceSession;

public class ParseXml {

	// -----2016jan---// //public ArrayList<ArrayList<String>> myHandler(String
	// str, Context context) throws JSONException {
	public static String Ar[] = { "", "", "", "", "" };

	public static void main(String[] args) throws IOException, InterruptedException, SAXException,
			ParserConfigurationException, ParseException, SQLException {
		// public static String myHandler(RestfulInfo restfulinfo, Context
		// context) throws JSONException, IOException, InterruptedException,
		// SAXException, ParserConfigurationException, SQLException,
		// ParseException {
		// The following statement is for testing by pass string parameter as
		// the input
		// public static void myHandler(String str, Context context) throws
		// InterruptedException, IOException {
		// -----------------------------------
		RestfulInfo restfulinfo = new RestfulInfo();
		restfulinfo.setAccount("740e770bf4cf981c20bac2b72431ac2e");
		restfulinfo.setPassword("d063948120f4577795687a45d82a066f");
		restfulinfo.setDblink("jdbc:mysql://vl-aurora1.cdpcz0fpc2s4.us-east-1.rds.amazonaws.com:3306");
		restfulinfo.setBucketname("testmiddlewaredata");
		restfulinfo.setDbname("VLWebService");
		restfulinfo.setDbusername("sqladmin");
		restfulinfo.setDbpassword("Virt2ual!");
		// restfulinfo.setTablename( "WebOrderAddresses");
		// restfulinfo.setTablename2( "WebOrderHeader");
		// restfulinfo.setTablename3("WebOrderDetail");
		// restfulinfo.setRestfulres("Aws_S3_ShopifyOrder.xml");
		// restfulinfo.setCustomername("DANESON");
		// restfulinfo.setSessionid("911911911911");
		// restfulinfo.setSource("Shopify");

		// restfulinfo.setFulfillment_status("unshipped");

		// restfulinfo.setType("Shopify1.1getOrders");

		// System.out.println( restfulinfo.getDbusername());

		/*
		 * { "account": "740e770bf4cf981c20bac2b72431ac2e", "username":
		 * "427983567732", "password": "d063948120f4577795687a45d82a066f",
		 * "userkey": "VLIsAVeryWonderfulCompany", "region": "us-east-1",
		 * "fullfillment_statu": "unshipped", "restful_statu": "open", "limit":
		 * "250", "pages":"1", "restfulres": "Aws_S3_ShopifyOrder.xml",
		 * "updatetime_start": "2016-04-19T00:00:00-00:00", "updatetime_end":
		 * "2016-04-20T00:00:00-00:00", "bucketname": "mingquantest2", "dblink":
		 * "jdbc:mysql://vl-aurora1.cdpcz0fpc2s4.us-east-1.rds.amazonaws.com:3306;",
		 * "dbname": "VLWebService", "dbusername": "sqladmin", "dbpassword":
		 * "Virt2ual!", "tablename": "WebOrderAddresses", "tablename2":
		 * "WebOrderHeader", "tablename3": "WebOrderDetail" }
		 */

		// String str="RequestBL_Inventory";

		/*
		 * AWSCredentials credentials = null; try { credentials = new
		 * ProfileCredentialsProvider("default").getCredentials(); } catch
		 * (Exception e) { throw new AmazonClientException(
		 * "Cannot load the credentials from the credential profiles file. " +
		 * "Please make sure that your credentials file is at the correct " +
		 * "location (C:\\Users\\ascheper\\.aws\\credentials), and is in valid format."
		 * , e); }
		 */

		System.out.println("-------Start Parsing Nested XML File Shopify Order----------");
		AWSCredentials credentials = new BasicAWSCredentials("AKIAJIXXNF5WVWHERLCA",
				"Ax6qdkT4cHXu3JxEHARe8Rq+c9WE+hpzqWzfNkfY");

		AmazonS3 s3 = new AmazonS3Client(credentials);
		/*
		 * Region usr1 = Region.getRegion(Regions.US_EAST_1);
		 * s3.setRegion(usr1);
		 */

		String bucketName;

		String tablename;
		String webappId;
		String customId;

		String soapResponseXml = "";
		/*
		 * String bucketname2= "mingquantest3"; String ff="command2.txt";
		 * S3Object object0 = s3.getObject(new
		 * GetObjectRequest(bucketname2,ff));
		 * readTextInputStream(object0.getObjectContent());
		 * soapResponseXml=Ar[0];
		 * 
		 * webappId=Ar[1]; customId=Ar[2]; tablename=Ar[3];
		 */

		// Region usr2 = Region.getRegion(Regions.US_WEST_2);
		Region usr2 = Region.getRegion(Regions.US_EAST_1);
		s3.setRegion(usr2);

		// S3Object object2 = s3.getObject(new GetObjectRequest(bucketName,
		// soapResponseXml));
		// S3Object object2 = s3.getObject(new GetObjectRequest("mingquantest2",
		// "Aws_S3_ShopifyOrder.xml"));

		String path = restfulinfo.getBucketname();
		// System.out.println(path);
		S3Object object2 = s3.getObject(new GetObjectRequest(path, "WebService_data_by_session.xml"));
		ParseT p = new ParseT(); // Parse xml
		// RecursiveDOM rd= new RecursiveDOM(object2.getObjectContent());
		// List<Order> orderList =
		System.out.println("start...");
		List<XMLItem> sessionList = p.readFile(object2.getObjectContent(), "WebService_data_by_session.xml");// get
																												// the
																												// result

		for (XMLItem item : sessionList) {
			// System.out.println(item);
		}
		// System.out.println(sessionList.get(0));
		// Object to XML Conversion
		// XStream xstream = new XStream(new StaxDriver());
		// String xml = xstream.toXML(sessionList.get(0));
		// System.out.println(formatXml(xml));

		// XML to Object Conversion
		// WebServiceSession ord = ( WebServiceSession)xstream.fromXML(xml);
		// System.out.println(ord);
		// List<Order> orderList2 = new ArrayList<Order>();

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Canada/Eastern"));
		Date date = new Date();
		String dateStr = dateFormat.format(date);
		System.out.println("********" + dateStr + "*********");
		// System.out.println(orderList);
		// System.out.println(orderList.get(1).getEmail());
		// into the parse result data to database
		// Insert2Db in = new
		// Insert2Db(restfulinfo.getDblink()+"/"+restfulinfo.getDbname(),
		// restfulinfo.getDbusername(),restfulinfo.getDbpassword() );

		// in.set(orderList,
		// "jdbc:sqlserver://localhost:1433;","ECSWebService","sa","Virt2ual!",
		// "WebOrderAddresses");
		// in.set(orderList,restfulinfo.getDblink(),restfulinfo.getDbname(),restfulinfo.getDbusername(),restfulinfo.getDbpassword(),restfulinfo.getTablename());
		// in.set(orderList,
		// "jdbc:mysql://vl-aurora1.cdpcz0fpc2s4.us-east-1.rds.amazonaws.com:3306;","VLWebService","sqladmin","Virt2ual!",
		// "WebOrderAddresses");
		// try {
		// in.insertOrder(restfulinfo, orderList);
		// Test Insert
		// Insert2Db insert = new
		// Insert2Db("jdbc:mysql://vl-aurora1.cdpcz0fpc2s4.us-east-1.rds.amazonaws.com:3306/VLWebService","sqladmin","Virt2ual!");
		// Insert2Db insert = new
		// Insert2Db("jdbc:mysql://vl-aurora1.cdpcz0fpc2s4.us-east-1.rds.amazonaws.com:3306/VLWebService","sqladmin","Virt2ual!");
		// insert.insertOrder(restfulinfo, orderList);

		// } catch (InterruptedException e) {
		// TODO Auto-generated catch block
		//
		// e.printStackTrace();
		// }

		System.out.println("-------Finishing Parsing Nested XML File Shopify Order----------");
		
		
		try {
			// in.insertOrder(restfulinfo, orderList);
			// Test Insert
			// Insert2Db insert = new
			// Insert2Db("jdbc:mysql://vl-aurora1.cdpcz0fpc2s4.us-east-1.rds.amazonaws.com:3306/VLWebService","sqladmin","Virt2ual!");
			Insert2Db insert = new Insert2Db();
			insert.insertEntries(sessionList, restfulinfo);

		} catch (Exception e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
		// return "Success";
		// GetLambdaLogData glld= new GetLambdaLogData();
		// String pp = glld.GetLambdaBillTime(context.getLogGroupName(),
		// context.getLogStreamName());
		/*
		 * DatabaseConnector dbc = new DatabaseConnector(); // dbc.set(dbName,
		 * driver, dbUsername, dbPassword, dbHostname, port); List<Object>
		 * LambdaLogList = new ArrayList<>(); String JsonData =
		 * " {  \"LogStreamName\":\"" + context.getLogStreamName() + "  \" , " +
		 * " \"LambdaFunctionName\":\"" + context.getLogGroupName() + "  \" , "
		 * + " \"Duration\":     \"   null \" , " +
		 * " \"Billed_Duration\":     \"  null \" , " +
		 * " \"MaxMemoryUsed\":     \"  null \" , " + " \"SessionID\": "
		 * +Integer.valueOf(0) + " , } ";
		 * 
		 * 
		 * 
		 * dbc.insert(JsonData,"WebServiceLambda");
		 * 
		 */
		// return context.getLogGroupName()+ ";" + context.getLogStreamName() +
		// ";" + context.getAwsRequestId();

	}
	// --- }

	// change InputStream object to String object

	private static String[] readTextInputStream(InputStream input) throws IOException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		int i = 0;
		while (true) {
			String line = reader.readLine();
			if (line == null)
				break;

			// System.out.println(" " + line);
			Ar[i] = line.toString();
			// System.out.println(" " + Ar[i]);
			i = i + 1;

		}
		System.out.println();
		return Ar;
	}

	public static String formatXml(String xml) {

		try {
			Transformer serializer = SAXTransformerFactory.newInstance().newTransformer();

			serializer.setOutputProperty(OutputKeys.INDENT, "yes");
			serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			Source xmlSource = new SAXSource(new InputSource(new ByteArrayInputStream(xml.getBytes())));
			StreamResult res = new StreamResult(new ByteArrayOutputStream());

			serializer.transform(xmlSource, res);

			return new String(((ByteArrayOutputStream) res.getOutputStream()).toByteArray());

		} catch (Exception e) {
			return xml;
		}
	}

	public static void assertXMLEquals(String expectedXML, String actualXML) throws Exception {
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreAttributeOrder(true);

		DetailedDiff diff = new DetailedDiff(XMLUnit.compareXML(expectedXML, actualXML));

		List<?> allDifferences = diff.getAllDifferences();
		System.out.println(allDifferences.size());
		// Assert.assertEquals("Differences found: "+ diff.toString(), 0,
		// allDifferences.size());
	}

}
