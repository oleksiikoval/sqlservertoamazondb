package sqlServerToAmazonDB.MainApp;

 

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Public class to get a string from an inputStream, for use in parsing XML Docs
 * @author Ming Quan, modified by Connor Sortome
 *
 */
public class StreamConverter {
	
	/**
	 * Returns a string containing the contents of the inputStream.
	 * @param is InputStream containing the contents of the file
	 * @return String containing the contents of the file
	 */
	public String getString(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}
}
