package sqlServerToAmazonDB.MainApp;

/*This is a core java program application which is also a Lambda Function(related to AWS Lambda API)
 * ParseT is to call MyHandler0.java To parse nested Order Xml files
 * This program can be generalized, but now, fixed to Shopify order xml file
 * Created by Ming Quan Fu 
 * Version 1.0
 * Virtual Logistics Inc.
 */  

 // package name 
// Parse the nested XML file
//Call MyHandler0 to parse <Orders> 

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

 
import sqlServerToAmazonDB.getSessionOrders.XMLItem;
import sqlServerToAmazonDB.getSessionOrders.WebOrderAddresses;

import sqlServerToAmazonDB.getSessionOrders.WebOrderDetail;

import sqlServerToAmazonDB.getSessionOrders.WebOrderHeader;

import sqlServerToAmazonDB.getSessionOrders.WebServiceDTS;

import sqlServerToAmazonDB.getSessionOrders.WebServiceSession;
 
/**
 * Initiates parsing, with Handler
  
 *
 */
public class ParseT {
	/**
	 * Starts the XML parser based on the filename, then returns the resulting List object.
	 * @param inputstream
	 * @return List<Order> containing the results of the parsing
	 */
	public List<XMLItem> readFile(InputStream inputstream, String filename) throws InterruptedException{
		/* Declare an Order List, every element is one Order Object to store the parse result */
    	List<XMLItem> sessionList = new ArrayList<XMLItem>(); 	
    	/*This is for testing time only, We will not calculate the time here, but in Lambda log, so comment it, keep for testing goals */
	    // long startTime = System.currentTimeMillis();
	    //List<Product> prodList2 = new ArrayList<Product>();
	    SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
	    
	    try {
	        saxParserFactory.newSAXParser();
	        // System.out.println("enen");
	        /* Change the InputStream to String to make sure we can control the pointer to the position we want*/
	        StreamConverter cits = new StreamConverter();
			String inputxmlstring = cits.getString(inputstream);
	        System.out.println(inputxmlstring);
	    	/*When change a new one, we can change the "Order" as another variable
	    	 *the parse will start from MyHandler0 start
	    	 */
    	   
	    //	if(filename.toLowerCase().contains("session")){
	    		System.out.println("Parsing Sessions...");
		    	SAXParserFactory factory = SAXParserFactory.newInstance();  
		    	SAXParser parser = factory.newSAXParser();
		        MyHandler0 handler = new MyHandler0("WebServiceSession",inputxmlstring, new WebServiceSession());
		        System.out.println(inputxmlstring);
		        parser.parse(new InputSource(new StringReader(inputxmlstring)), handler);
		        sessionList = handler.getItems();  
		        if(handler.checkNumberParsed())   
		        	System.out.println(String.format("Finished parsing XML, item count %d parsed", sessionList.size()));
		        else
		        	System.out.println(String.format("Finished parsing XML, item count %d d not parsed", sessionList.size()));
	    	//}
	    } 
	    catch (SAXException e) { 
	    	//TODO Production code should handle more gracefully
	    	e.printStackTrace();
	    }   
	    catch(ParserConfigurationException e){} catch (IOException e) {
			// TODO Production code should handler more gracefully   
			e.printStackTrace();
		}
	    
		return sessionList;
    }
}