package sqlServerToAmazonDB.MainApp;

 

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import sqlServerToAmazonDB.getSessionOrders.*;
 

public class OrderChecker {
	private String sessionID;
	private boolean verbose = false;
	private Connection conn;
	private String pokey;

	//TODO Update this so we don't have hard-coded DB connections everywhere
	public OrderChecker(String SessionID){
		this.sessionID = SessionID;

		try {
			String driver = "com.mysql.jdbc.Driver";

			Class.forName(driver).newInstance();

			String jdbcUrl = "jdbc:mysql://vl-aurora1.cdpcz0fpc2s4.us-east-1.rds.amazonaws.com:3306/VLWebService?user=sqladmin&password=Virt2ual!";

			conn = DriverManager.getConnection(jdbcUrl);

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Sets the output level for the print statements. If false, prints only non-matching fields, notifications will be printed for all fields.
	 * @param verbose Boolean value for the output level.
	 */
	public void setVerbose(boolean verbose){
		this.verbose = verbose;
	}

	/**
	 * For each table being inserted into the database from data, we check the fields for each entry in the database
	 * @param itemList the list of items parsed from a XML doc
	 */
	public void compareList(List<XMLItem> itemList){
		String key;
		//---------------------------------------------------------------------------------------------------------------------------------\\
		//-----------------------------------------------FOR COMPARING DATA BY SESSION-----------------------------------------------------\\
		//---------------------------------------------------------------------------------------------------------------------------------\\
		try {
			int index = 0;
			for(XMLItem sessionTable : itemList){
				if(!((WebServiceSession) sessionTable).getCustomerID().equals("null")){

					HashMap<String, Integer> map = new HashMap<String, Integer>();
					for(XMLItem item : ((WebServiceSession) sessionTable).getWebOrderHeader()) {
						WebOrderHeader order = (WebOrderHeader)item;
						map.put(order.getPOKEY()+order.getWebOrderNumber()+order.getWebOrderID(), index++);
					}
					index = 0;

					//----------------------------------------------------------------------------------------------------------------------\\
					//---------------------------------------FOR COMPARING WEBORDERHEADER TABLE---------------------------------------------\\
					//----------------------------------------------------------------------------------------------------------------------\\
					System.out.println("Comparing WebOrderHeader table");
					PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM WebOrderHeader WHERE LegacySessionID = ?");
					pstmt.setInt(1, Integer.valueOf(sessionID));

					ResultSet rs = pstmt.executeQuery();

					//Get the RowCount from the ResultSet
					rs.last();
					int rowcount = rs.getRow();
					rs.first();

					if(rowcount>0)
						do{
							try{
								//Assign key value so we grab the right item from the arraylist
								key = rs.getString("POKEY")+rs.getString("WebOrderNumber")+rs.getString("WebOrderID");
								//Get the table reference to compare to
								WebOrderHeader toCompare = (WebOrderHeader) ((WebServiceSession) sessionTable).getWebOrderHeader().get(map.get(key));

								pokey = rs.getString("POKEY");
								System.out.println(String.format("Comparing WebOrderHeader %d of %d  %s", rs.getRow(), rowcount, pokey));
								//Compare CustomerID
								compareEntries(toCompare.getCustomerID(), rs.getString("CustomerID"), "CustomerID");
								//Compare WebOrderNumber
								compareEntries(toCompare.getWebOrderNumber(), rs.getString("WebOrderNumber"), "WebOrderNumber");
								//Compare WebOrderID
								compareEntries(toCompare.getWebOrderID(), rs.getString("WebOrderID"), "WebOrderID" );
								//Compare SalesOrderID
								compareEntries(toCompare.getSalesOrderId(), rs.getString("SalesOrderID"), "SalesOrderID" );
								//Compare InvoiceNumber
								compareEntries(toCompare.getInvoiceNumber(), rs.getString("InvoiceNumber"), "InvoiceNumber" );
								//OrderDate
								compareEntries(String.valueOf(toCompare.getOrderDate()), rs.getString("OrderDate"), "OrderDate" );
								//CreatedDate
								compareEntries(String.valueOf(toCompare.getCreatedDate()), rs.getString("CreatedDate"), "CreatedDate" );
								//InvoiceDate
								compareEntries(String.valueOf(toCompare.getInvoiceDate()), rs.getString("InvoiceDate"), "InvoiceDate" );
								//OrderStatus
								compareEntries(toCompare.getOrderStatus(), rs.getString("OrderStatus"), "OrderStatus" );
								//FulfilmentStatus
								compareEntries(toCompare.getFulfilment_Status(), rs.getString("Fulfilment_Status"), "FulfillmentStatus" );
								//FirstName
								compareEntries(toCompare.getFirstName(), rs.getString("FirstName"), "FirstName" );
								//LastName
								compareEntries(toCompare.getLastName(), rs.getString("LastName"), "LastName" );
								//Salutation
								compareEntries(toCompare.getSalutation(), rs.getString("Salutation"), "Salutation" );
								//WebStore
								compareEntries(toCompare.getWebstore(), rs.getString("WebStore"), "WebStore" );
								//OrderType
								compareEntries(toCompare.getOrderType(), rs.getString("OrderType"), "OrderType" );
								//Currency
								compareEntries(toCompare.getCurrency(), rs.getString("Currency"), "Currency" );
								//Vendor
								compareEntries(toCompare.getVendor(), rs.getString("Vendor"), "Vendor" );
								//AppId
								compareEntries(toCompare.getAppId(), rs.getString("AppId"), "AppId" );
								//Deptno
								compareEntries(toCompare.getDeptno(), rs.getString("Deptno"), "Deptno" );
								//EdiDeptNo
								compareEntries(toCompare.getEDI_DeptNo(), rs.getString("EDI_DeptNo"), "EDI_DeptNo" );
								//ShipTo
								compareEntries(toCompare.getShipTo(), rs.getString("ShipTo"), "ShipTo" );
								//ShipMethod
								compareEntries(toCompare.getShipMethod(), rs.getString("ShipMethod"), "ShipMethod" );
								//ShipMethodDesc
								compareEntries(toCompare.getShipMethodDesc(), rs.getString("ShipMethodDesc"), "ShipMethodDesc" );
								//TrackingNumber
								compareEntries(toCompare.getTrackingNumber(), rs.getString("TrackingNumber"), "TrackingNumber" );
								//ShipmentId
								compareEntries(toCompare.getShipmentId(), rs.getString("ShipmentId"), "ShipmentId" );
								//ShipmentWeightUOM
								compareEntries(toCompare.getShipmentWeightUOM(), rs.getString("ShipmentWeightUOM"), "ShipmentWeightUOM" );
								//UnitShipped
								compareEntries(String.valueOf(toCompare.getUnitShipped()), rs.getString("UnitShipped"), "UnitShipped" );
								//ShipmentWeight
								compareEntries(String.valueOf(toCompare.getShipmentWeight()), rs.getString("ShipmentWeight"), "ShipmentWeight" );
								//BOL
								compareEntries(toCompare.getBOL(), rs.getString("BOL"), "BOL" );
								//ShipDate
								compareEntries(String.valueOf(toCompare.getShipDate()), rs.getString("ShipDate"), "ShipDate" );
								//ShippingCost
								compareEntries(String.valueOf(toCompare.getShippingCost()), rs.getString("ShippingCost"), "ShippingCost" );
								//EventCode
								compareEntries(String.valueOf(toCompare.getEventCode()), rs.getString("EventCode"), "EventCode" );
								//BlanketNo
								compareEntries(String.valueOf(toCompare.getBlanketNo()), rs.getString("BlanketNo"), "BlanketNo" );
								//TermType
								compareEntries(toCompare.getTermType(), rs.getString("TermType"), "TermType" );
								//TermBasisDate
								compareEntries(toCompare.getTermBasisDate(), rs.getString("TermBasisDate"), "TermBasisDate" );
								//TermsAmt
								compareEntries(String.valueOf(toCompare.getTermsAmt()), rs.getString("TermsAmt"), "TermsAmt" );
								//TermsDiscDue
								compareEntries(String.valueOf(toCompare.getTermsDiscDue()), rs.getString("TermsDiscDue"), "TermsDiscDue" );
								//TermsNetDue
								compareEntries(String.valueOf(toCompare.getTermsNetDue()), rs.getString("TermsNetDue"), "TermsNetDue" );
								//RequestShipDate
								compareEntries(String.valueOf(toCompare.getRequestShipDate()), rs.getString("RequestShipDate"), "RequestShipDate" );
								//ShipNotB4Date
								compareEntries(String.valueOf(toCompare.getShipNotB4Date()), rs.getString("ShipNotB4Date"), "ShipNotB4Date" );
								//CancelAfterDate
								compareEntries(String.valueOf(toCompare.getCancelAfterDate()), rs.getString("CancelAfterDate"), "CancelAfterDate" );
								//ReqDeliveryDate
								compareEntries(String.valueOf(toCompare.getReqDeliveryDate()), rs.getString("ReqDeliveryDate"), "ReqDeliveryDate" );
								//InvAlias
								compareEntries(toCompare.getInvAlias(), rs.getString("InvAlias"), "InvAlias" );
								//Promo
								compareEntries(toCompare.getPromo(), rs.getString("Promo"), "Promo" );
								//Type
								compareEntries(toCompare.getType(), rs.getString("Type"), "Type" );
								//ImportDate
								compareEntries(String.valueOf(toCompare.getImportDate()), rs.getString("ImportDate"), "ImportDate" );
								//BackOrder
								compareEntries(String.valueOf(toCompare.getBackOrder()), rs.getString("BackOrder"), "BackOrder" );
								//ShipNow
								compareEntries(String.valueOf(toCompare.getShipNow()), rs.getString("ShipNow"), "ShipNow" );
								//PrintNow
								compareEntries(String.valueOf(toCompare.getPrintNow()), rs.getString("PrintNow"), "PrintNow" );
								//ExportNow
								compareEntries(String.valueOf(toCompare.getExportNow()), rs.getString("ExportNow"), "ExportNow" );
								//Exported
								compareEntries(String.valueOf(toCompare.getExported()), rs.getString("Exported"), "Exported" );

								//Remove the key from the map so we don't run into it again
								map.remove(key);

							} catch (SQLException e) {
								System.out.print("You have an error in your SQL syntax, check the log");
								e.printStackTrace();
							} catch (IndexOutOfBoundsException e) {
								System.out.println("Database returned more entries than the list??? Check identifier");
								e.printStackTrace();
							}
						}while(rs.next());
					//Empty the map
					map.clear();

					//----------------------------------------------------------------------------------------------------------------------\\
					//-------------------------------------FOR COMPARING WEBORDERADDRESSES TABLE--------------------------------------------\\
					//----------------------------------------------------------------------------------------------------------------------\\
					System.out.println("Comparing WebOrderAddresses table");
					map = new HashMap<String, Integer>();
					for(XMLItem item : ((WebServiceSession) sessionTable).getWebOrderAddresses()) {
						WebOrderAddresses order = (WebOrderAddresses)item;
						map.put(order.getPOKey()+order.getAcctCode()+order.getCustomerCode(), index++);
					}
					index = 0;

					pstmt = conn.prepareStatement("SELECT * FROM WebOrderAddresses WHERE LegacySessionID = ?");
					pstmt.setString(1, sessionID);

					rs = pstmt.executeQuery();

					//Get the RowCount from the ResultSet
					rs.last();
					rowcount = rs.getRow();
					rs.first();

					if(rowcount>0)
						do{
							try{
								//Assign key value so we grab the right item from the arraylist
								key = rs.getString("POKey")+rs.getString("AcctCode")+rs.getString("CustomerCode");
								//Get the table reference to compare to
								WebOrderAddresses toCompare = (WebOrderAddresses) ((WebServiceSession) sessionTable).getWebOrderHeader().get(map.get(key));

								pokey = rs.getString("POKey");
								System.out.println(String.format("Comparing WebOrderAddresses %d of %d  %s", rs.getRow(), rowcount, pokey));
								//Compare CustomerID
								compareEntries(String.valueOf(toCompare.getCustomerID()), rs.getString("CustomerID"), "CustomerID" );
								//Compare AddressCode
								compareEntries(String.valueOf(toCompare.getAddressCode()), rs.getString("AddressCode"), "AddressCode" );
								//Compare AddressType
								compareEntries(String.valueOf(toCompare.getAddressType()), rs.getString("AddressType"), "AddressType" );
								//Compare POKey
								compareEntries(String.valueOf(toCompare.getPOKey()), rs.getString("POKey"), "POKey" );
								//Compare SessionID
								compareEntries(String.valueOf(toCompare.getSessionID()), rs.getString("SessionID"), "SessionID" );
								//Compare Name
								compareEntries(String.valueOf(toCompare.getName()), rs.getString("Name"), "Name" );
								//Compare Name1
								compareEntries(String.valueOf(toCompare.getName1()), rs.getString("Name1"), "Name1" );
								//Compare Address1
								compareEntries(String.valueOf(toCompare.getAddress1()), rs.getString("Address1"), "Address1" );
								//Compare Address2
								compareEntries(String.valueOf(toCompare.getAddress2()), rs.getString("Address2"), "Address2" );
								//Compare Address3
								compareEntries(String.valueOf(toCompare.getAddress3()), rs.getString("Address3"), "Address3" );
								//Compare Address4
								compareEntries(String.valueOf(toCompare.getAddress4()), rs.getString("Address4"), "Address4" );
								//Compare Address5
								compareEntries(String.valueOf(toCompare.getAddress5()), rs.getString("Address5"), "Address5" );
								//Compare City
								compareEntries(String.valueOf(toCompare.getCity()), rs.getString("City"), "City" );
								//Compare Province
								compareEntries(String.valueOf(toCompare.getProvince()), rs.getString("Province"), "Province" );
								//Compare PostalCode
								compareEntries(String.valueOf(toCompare.getPostalCode()), rs.getString("PostalCode"), "PostalCode" );
								//Compare Country
								compareEntries(String.valueOf(toCompare.getCountry()), rs.getString("Country"), "Country" );
								//Compare Phone
								compareEntries(String.valueOf(toCompare.getPhone()), rs.getString("Phone"), "Phone" );
								//Compare AcctCode
								compareEntries(String.valueOf(toCompare.getAcctCode()), rs.getString("AcctCode"), "AcctCode" );
								//Compare Status
								compareEntries(String.valueOf(toCompare.getStatus()), rs.getString("Status"), "Status" );
								//Compare PrintQueue
								compareEntries(String.valueOf(toCompare.getPrintQueue()), rs.getString("PrintQueue"), "PrintQueue" );
								//Compare Misc1
								compareEntries(String.valueOf(toCompare.getMisc1()), rs.getString("Misc1"), "Misc1" );
								//Compare Misc2
								compareEntries(String.valueOf(toCompare.getMisc2()), rs.getString("Misc2"), "Misc2" );
								//Compare Misc3
								compareEntries(String.valueOf(toCompare.getMisc3()), rs.getString("Misc3"), "Misc3" );
								//Compare CustomerCode
								compareEntries(String.valueOf(toCompare.getCustomerCode()), rs.getString("CustomerCode"), "CustomerCode" );
								//Compare objID
								compareEntries(String.valueOf(toCompare.getObjID()), rs.getString("objID"), "objID" );
								//compare resource_URL
								compareEntries(String.valueOf(toCompare.getResource_URL()), rs.getString("resource_URL"), "resource_URL" );
								//compare customerGroupObjID
								compareEntries(String.valueOf(toCompare.getCustomerGroupOjbID()), rs.getString("customerGroupOjbID"), "customerGroupOjbID" );
								//compare customerGroupResourceURI
								compareEntries(String.valueOf(toCompare.getCustomerGroupResource_uri()), rs.getString("customerGroupResource_uri"), "customerGroupResource_uri" );
								//Compare user_groupID
								compareEntries(String.valueOf(toCompare.getUser_groupID()), rs.getString("user_groupID"), "user_groupID" );
								//Compare user_group_Resource_uri
								compareEntries(String.valueOf(toCompare.getUser_group_Resource_uri()), rs.getString("user_group_Resource_uri"), "user_group_Resource_uri" );
								//Compare MapsBIUpdate
								compareEntries(String.valueOf(toCompare.getMapsBIUpdate()), rs.getString("MapsBIUpdate"), "MapsBIUpdate" );
								//Compare Email
								compareEntries(String.valueOf(toCompare.getEmail()), rs.getString("Email"), "Email" );
								//Compare Company
								compareEntries(String.valueOf(toCompare.getCompany()), rs.getString("Company"), "Company" );
								//Compare LegacySessionID
								compareEntries(String.valueOf(toCompare.getSessionID()), rs.getString("LegacySessionID"), "LegacySessionID" );

								//Remove the key from the map so we don't run into it again
								map.remove(key);

							} catch (SQLException e) {
								System.out.print("You have an error in your SQL syntax, check the log");
								e.printStackTrace();
							} catch (IndexOutOfBoundsException e) {
								System.out.println("Database returned more entries than the list??? Check identifier");
								e.printStackTrace();
							}
						}while(rs.next());

					//Empty the map
					map.clear();

					//----------------------------------------------------------------------------------------------------------------------\\
					//---------------------------------------FOR COMPARING WEBORDERDETAIL TABLE---------------------------------------------\\
					//----------------------------------------------------------------------------------------------------------------------\\
					System.out.println("Comparing WebOrderDetail table");
					map = new HashMap<String, Integer>();
					for(XMLItem item : ((WebServiceSession) sessionTable).getWebOrderDetail()) {
						WebOrderDetail order = (WebOrderDetail)item;
						map.put((order.getPOKEY()+order.getPOLine()), index++);
					}
					index = 0;

					pstmt = conn.prepareStatement("SELECT * FROM WebOrderDetail WHERE LegacySessionID = ?");
					pstmt.setString(1, sessionID);

					rs = pstmt.executeQuery();

					//Get the RowCount from the ResultSet
					rs.last();
					rowcount = rs.getRow();
					rs.first();

					if(rowcount>0)
						do{
							try{
								//Assign key value so we grab the right item from the arraylist
								key = rs.getString("POKEY")+rs.getString("POLine");
								//Get the table reference to compare to
								WebOrderDetail toCompare = (WebOrderDetail) ((WebServiceSession) sessionTable).getWebOrderDetail().get(map.get(key));

								pokey = rs.getString("POKEY");
								System.out.println(String.format("Comparing WebOrderDetail %d of %d  %s", rs.getRow(), rowcount, pokey));
								//Compare POKEY
								compareEntries(String.valueOf(toCompare.getPOKEY()), rs.getString("POKEY"), "POKEY" );
								//Compare poline
								compareEntries(String.valueOf(toCompare.getPOLine()), rs.getString("POLine"), "POLine" );
								//Compare customer_PO_Line
								compareEntries(String.valueOf(toCompare.getCustomer_PO_Line()), rs.getString("Customer_PO_Line"), "Customer_PO_Line");
								//Compare Sequence
								compareEntries(String.valueOf(toCompare.getSequence()), rs.getString("Sequence"), "Sequence");
								//Compare OrderQty
								compareEntries(String.valueOf(toCompare.getOrderQty()), String.valueOf(rs.getDouble("OrderQty")), "OrderQty");
								//Compare BackOrderQty
								compareEntries(String.valueOf(toCompare.getBackOrderQty()), rs.getString("BackOrderQty"), "backOrderQty");
								//Compare ShipQty
								compareEntries(String.valueOf(toCompare.getShipQty()), String.valueOf(rs.getDouble("ShipQty")), "ShipQty");
								//Compare ShippedQty
								compareEntries(String.valueOf(toCompare.getShippedQty()), String.valueOf(rs.getDouble("ShippedQty")), "ShippedQty");
								//Compare InvoicedQty
								compareEntries(String.valueOf(toCompare.getInvoicedQty()), String.valueOf(rs.getDouble("InvoicedQty")), "InvoicedQty");
								//Compare UOM
								compareEntries(String.valueOf(toCompare.getUOM()), rs.getString("UOM"), "UOM");
								//Compare EDI_UOM
								compareEntries(String.valueOf(toCompare.getEDI_UOM()), rs.getString("EDI_UOM"), "EDI_UOM");
								//Compare UnitPrice
								compareEntries(String.valueOf(toCompare.getUnitPrice()), rs.getString("UnitPrice"), "UnitPrice");
								//Compare RetailPrice
								compareEntries(String.valueOf(toCompare.getRetailPrice()), rs.getString("RetailPrice"), "RetailPrice");
								//Compare DiscountValue
								compareEntries(String.valueOf(toCompare.getDiscountValue()), rs.getString("DiscountValue"), "DiscountValue");
								//Compare SKU
								compareEntries(String.valueOf(toCompare.getSKU()), rs.getString("SKU"), "SKU");
								//Compare Customer SKU
								compareEntries(String.valueOf(toCompare.getCustomerSKU()), rs.getString("CustomerSKU"), "CustomerSKU");
								//Compare UPC
								compareEntries(String.valueOf(toCompare.getUPC()), rs.getString("UPC"), "UPC");
								//Compare AlternateID
								compareEntries(String.valueOf(toCompare.getAlternateID()), rs.getString("AlternateID"), "AlternateID");
								//Compare Style
								compareEntries(String.valueOf(toCompare.getStyle()), rs.getString("Style"), "Style");
								//Compare Colour 
								compareEntries(String.valueOf(toCompare.getColour()), rs.getString("Colour"), "Colour");
								//Compare Size
								compareEntries(String.valueOf(toCompare.getSize()), rs.getString("Size"), "Size");
								//Compare InSeam
								compareEntries(String.valueOf(toCompare.getInseam()), rs.getString("Inseam"), "Size");
								//Compare Description
								compareEntries(String.valueOf(toCompare.getDescription()), rs.getString("Description"), "Description");
								//ShipDate_Detail
								compareEntries(String.valueOf(toCompare.getShipDate_Detail()), rs.getString("ShipDate_Detail"), "ShipDate_Detail");
								//Compare ExtraField1
								compareEntries(String.valueOf(toCompare.getExtraField1()), rs.getString("ExtraField1"), "ExtraField1");
								//Compare ExtraField2
								compareEntries(String.valueOf(toCompare.getExtraField2()), rs.getString("ExtraField2"), "ExtraField2");
								//Compare ExtraField3
								compareEntries(String.valueOf(toCompare.getExtraField3()), rs.getString("ExtraField3"), "ExtraField3");
								//Compare ExtraField4
								compareEntries(String.valueOf(toCompare.getExtraField4()), rs.getString("ExtraField4"), "ExtraField4");
								//Compare ExtraField5
								compareEntries(String.valueOf(toCompare.getExtraField5()), rs.getString("ExtraField5"), "ExtraField5");
								//Compare ExtraField6
								compareEntries(String.valueOf(toCompare.getExtraField6()), rs.getString("ExtraField6"), "ExtraField6");
								//Compare ExtraField7
								compareEntries(String.valueOf(toCompare.getExtraField7()), rs.getString("ExtraField7"), "ExtraField7");
								//Compare ExtraField8
								compareEntries(String.valueOf(toCompare.getExtraField8()), rs.getString("ExtraField8"), "ExtraField8");
								//Compare ExtraField9
								compareEntries(String.valueOf(toCompare.getExtraField9()), rs.getString("ExtraField9"), "ExtraField9");
								//Compare ExtraField10
								compareEntries(String.valueOf(toCompare.getExtraField10()), rs.getString("ExtraField10"), "ExtraField10");
								//Compare ItemStatus
								compareEntries(String.valueOf(toCompare.getItemStatus()), rs.getString("ItemStatus"), "ItemStatus");
								//Compare ItemNotes
								compareEntries(String.valueOf(toCompare.getItemNotes()), rs.getString("ItemNotes"), "ItemNotes");
								//Compare HubspotPosted
								compareEntries(String.valueOf(toCompare.getHubspotPosted()), rs.getString("HubspotPosted"), "HubspotPosted");
								//Compare HubSpotPostedDateTime
								compareEntries(String.valueOf(toCompare.getHubspotPostedDatetime()), rs.getString("HubspotPostedDateTime"), "HubspotPostedDateTime");
								//Compare HubspotPostedAttempt
								compareEntries(String.valueOf(toCompare.getHubspotPostedAttempt()), rs.getString("HubspotPostedAttempt"), "HubspotPostedAttempt");
								//Compare TrackingNumber
								compareEntries(String.valueOf(toCompare.getTrackingNumber()), rs.getString("TrackingNumber"), "TrackingNumber");
								//Compare SCC18
								compareEntries(String.valueOf(toCompare.getSCC18()), rs.getString("SCC18"), "SCC18");
								//Compare Pack
								compareEntries(String.valueOf(toCompare.getPack()), rs.getString("Pack"), "Pack");
								//Compare innerPack
								compareEntries(String.valueOf(toCompare.getInnerpack()), rs.getString("innerPack"), "innerPack");
								//Compare Tax
								compareEntries(String.valueOf(toCompare.getTax()), rs.getString("Tax"), "Tax");
								//Compare Tax2
								compareEntries(String.valueOf(toCompare.getTax2()), rs.getString("Tax2"), "Tax2");
								//Compare Ultrec
								compareEntries(String.valueOf(toCompare.getUltrec()), rs.getString("Ultrec"), "Ultrec");
								//Compare VendorStyle
								compareEntries(String.valueOf(toCompare.getVendorStyle()), rs.getString("VendorStyle"), "VendorStyle");
								//Compare PackagingSpecNumber
								compareEntries(String.valueOf(toCompare.getPackagingSpecNumber()), rs.getString("PackagingSpecNumber"), "PackagingSpecNumber");
								//Compare BuyerItemNumber
								compareEntries(String.valueOf(toCompare.getBuyerItemNumber()), rs.getString("BuyerItemNumber"), "BuyerItemNumber");
								//Compare AssortabilityCode
								compareEntries(String.valueOf(toCompare.getAssortabilityCode()), rs.getString("AssortabilityCode"), "AssortabilityCode");
								//Compare MerchandiseCode
								compareEntries(String.valueOf(toCompare.getMerchandiseCode()), rs.getString("MerchandiseCode"), "MerchandiseCode");
								//Compare BuyerColour
								compareEntries(String.valueOf(toCompare.getBuyerColour()), rs.getString("BuyerColour"), "BuyerColour");
								//Compare Type
								compareEntries(String.valueOf(toCompare.getType()), rs.getString("Type"), "Type");
								//Compare TaxableAmount1
								compareEntries(String.valueOf(toCompare.getTaxableAmount1()), rs.getString("TaxableAmount1"), "TaxableAmount1");
								//Compare TaxableAmount2
								compareEntries(String.valueOf(toCompare.getTaxableAmount2()), rs.getString("TaxableAmount2"), "TaxableAmount2");
								//Compare LegacySessionID
								compareEntries(String.valueOf(toCompare.getSessionID()), rs.getString("LegacySessionID"), "LegacySessionID");

								//Remove the key from the map so we don't hit it again
								map.remove(key);

							} catch (SQLException e) {
								System.out.print("You have an error in your SQL syntax, check the log");
								e.printStackTrace();
							} catch (IndexOutOfBoundsException e) {
								System.out.println("Database returned more entries than the list??? Check identifier");
								e.printStackTrace();
							}
						}while(rs.next());
					//Empty the map
					map.clear();

					//----------------------------------------------------------------------------------------------------------------------\\
					//----------------------------------------FOR COMPARING WEBSERVICEDTS TABLE---------------------------------------------\\
					//----------------------------------------------------------------------------------------------------------------------\\
					System.out.println("Comparing WebServiceDTSTable");
					map = new HashMap<String, Integer>();
					System.out.println(((WebServiceSession) sessionTable).getWebServiceDTS().size());
					for(XMLItem item : ((WebServiceSession) sessionTable).getWebServiceDTS()) {
						WebServiceDTS order = (WebServiceDTS)item;
						map.put(order.getPOKEY()+order.getCustomerID()+order.getShipmentId(), index++);
					}
					
					index=0;

					pstmt = conn.prepareStatement("SELECT * FROM WebServiceDTS WHERE LegacySessionID = ?");
					pstmt.setString(1, sessionID);

					rs = pstmt.executeQuery();

					//Get the RowCount from the ResultSet
					rs.last();
					rowcount = rs.getRow();
					rs.first();
					System.out.println(rowcount);
					if(rowcount>0)
						do{
							try{
								//Assign key value so we grab the right item from the arraylist
								key = rs.getString("POKEY")+rs.getString("CustomerID")+rs.getString("ShipmentID");
								//Get the table reference to compare to
								WebServiceDTS toCompare = (WebServiceDTS) ((WebServiceSession) sessionTable).getWebOrderHeader().get(map.get(key));

								pokey = rs.getString("POKEY");
								System.out.println(String.format("Comparing WebServiceDTS %d of %d  %s", rs.getRow(), rowcount, pokey));
								//Compare CustomerID
								compareEntries(String.valueOf(toCompare.getCustomerID()), rs.getString("CustomerID"), "CustomerID");
								//Compare POKEY
								compareEntries(String.valueOf(toCompare.getPOKEY()), rs.getString("POKEY"), "POKEY");
								//Compare DocType1
								compareEntries(String.valueOf(toCompare.getDocType1()), rs.getString("DocType1"), "DocType1");
								//Compare DocType2
								compareEntries(String.valueOf(toCompare.getDocType2()), rs.getString("DocType2"), "DocType2");
								//Compare DocType3
								compareEntries(String.valueOf(toCompare.getDocType3()), rs.getString("DocType2"), "DocType3");
								//Compare DocReference1
								compareEntries(String.valueOf(toCompare.getDocReference1()), rs.getString("DocReference1"), "DocReference1");
								//Compare DocReference2
								compareEntries(String.valueOf(toCompare.getDocReference2()), rs.getString("DocReference2"), "DocReference2");
								//Compare DocReference3
								compareEntries(String.valueOf(toCompare.getDocReference3()), rs.getString("DocReference3"), "DocReference3");
								//Compare SourceID
								compareEntries(String.valueOf(toCompare.getSourceID()), rs.getString("SourceID"), "SourceID");
								//Compare Source
								compareEntries(String.valueOf(toCompare.getSource()), rs.getString("Source"), "Source");
								//Compare TargetID
								compareEntries(String.valueOf(toCompare.getTargetID()), rs.getString("TargetID"), "TargetID");
								//Compare Target
								compareEntries(String.valueOf(toCompare.getTarget()), rs.getString("Target"), "Target");
								//Compare ECSBatchID
								compareEntries(String.valueOf(toCompare.getECSBatchId()), rs.getString("ECSBatchID"), "ECSBatchID");
								//Compare SourceFile
								compareEntries(String.valueOf(toCompare.getSourceFile()), rs.getString("SourceFile"), "SourceFile");
								//Compare TargetFile
								compareEntries(String.valueOf(toCompare.getTargetFile()), rs.getString("TargetFile"), "TargetFile");
								//Compare ResponseFile
								compareEntries(String.valueOf(toCompare.getResponseFile()), rs.getString("ResponseFile"), "ResponseFile");
								//Compare CreationDateTime
								compareEntries(String.valueOf(toCompare.getCreationDateTime()), rs.getString("CreationDateTime"), "CreationDateTime");
								//Compare PostDateTime
								compareEntries(String.valueOf(toCompare.getPostDateTime()), rs.getString("PostDateTime"), "PostDateTime");
								//Compare ResponseDateTime
								compareEntries(String.valueOf(toCompare.getResponseDateTime()), rs.getString("ResponseDateTime"), "ResponseDateTime");
								//Compare Posted
								compareEntries(String.valueOf(toCompare.getPosted()), rs.getString("Posted"), "Posted");
								//Compare OrderStatus
								compareEntries(String.valueOf(toCompare.getOrderStatus()), rs.getString("OrderStatus"), "OrderStatus");
								//Compare StatusDescription
								compareEntries(String.valueOf(toCompare.getStatusDescription()), rs.getString("StatusDescription"), "StatusDescription");
								//Compare SourceNbItem
								compareEntries(String.valueOf(toCompare.getSourceNbItem()), rs.getString("SourceNbItem"), "SourceNbItem");
								//Compare SourceTotalQuantity
								compareEntries(String.valueOf(toCompare.getSourceTotalQuantity()), rs.getString("SourceTotalQuantity"), "SourceTotalQuantity");
								//Compare TargetNbItem
								compareEntries(String.valueOf(toCompare.getTargetNbItem()), rs.getString("TargetNbItem"), "TargetNbItem");
								//Compare TargetTotalQuantity
								compareEntries(String.valueOf(toCompare.getTargetTotalQuantity()), rs.getString("TargetTotalQuantity"), "TargetTotalQuantity");
								//Compare ShipBatchID
								compareEntries(String.valueOf(toCompare.getShipBatchId()), rs.getString("ShipBatchId"), "ShipBatchId");
								//Compare Shipped
								compareEntries(String.valueOf(toCompare.getShipped()), rs.getString("Shipped"), "Shipped");
								//Compare ShipmentID
								compareEntries(String.valueOf(toCompare.getShipmentId()), rs.getString("ShipmentId"), "ShipmentId");
								//Compare ShipTargetFile
								compareEntries(String.valueOf(toCompare.getShipTargetFile()), rs.getString("ShipTargetFile"), "ShipTargetFile");
								//Compare ShipSourceFile
								compareEntries(String.valueOf(toCompare.getShipSourceFile()), rs.getString("ShipSourceFile"), "ShipSourceFile");
								//Compare ShipResponseFile
								compareEntries(String.valueOf(toCompare.getShipResponseFile()), rs.getString("ShipResponseFile"), "ShipResponseFile");
								//Compare ShipGetDate
								compareEntries(String.valueOf(toCompare.getShipGetDate()), rs.getString("ShipGetDate"), "ShipGetDate");
								//Compare ShipPostDate
								compareEntries(String.valueOf(toCompare.getShipPostDate()), rs.getString("ShipPostDate"), "ShipPostDate");
								//Compare ShipPkgTraceId
								compareEntries(String.valueOf(toCompare.getShipPkgTraceId()), rs.getString("ShipPkgTraceId"), "ShipPkgTraceId");
								//Compare ShipDate
								compareEntries(String.valueOf(toCompare.getShipDate()), rs.getString("ShipDate"), "ShipDate");
								//Compare ShipMethod
								compareEntries(String.valueOf(toCompare.getShipMethod()), rs.getString("ShipMethod"), "ShipMethod");
								//Compare ShipStatus
								compareEntries(String.valueOf(toCompare.getShipStatus()), rs.getString("ShipStatus"), "ShipStatus");
								//Compare ShipSatusDesc
								compareEntries(String.valueOf(toCompare.getShipSatusDesc()), rs.getString("ShipStatusDesc"), "ShipSatus");
								//Compare Status
								compareEntries(String.valueOf(toCompare.getStatus()), rs.getString("ShipStatusDesc"), "ShipStatus");
								//Compare SatusNote
								compareEntries(String.valueOf(toCompare.getSatusNote()), rs.getString("ShipSatusNote"), "ShipSatus");
								//Compare StatusDate
								compareEntries(String.valueOf(toCompare.getStatusDate()), rs.getString("StatusDate"), "StatusDate");
								//Compare Conversation
								compareEntries(String.valueOf(toCompare.getConversation()), rs.getString("Conversation"), "Conversation");
								//Compare sessionLogFile
								compareEntries(String.valueOf(toCompare.getSessionLogFile()), rs.getString("sessionLogFile"), "sessionLogFile");
								//Compare TransactionType
								compareEntries(String.valueOf(toCompare.getTransactionType()), rs.getString("TransactionType"), "TransactionType");
								//Compare LegacySessionID
								compareEntries(String.valueOf(toCompare.getSessionID()), rs.getString("LegacySessionID"), "LegacySessionID");
								
								//Remove the key from the map so we don't run into it again
								map.remove(key);
								
							} catch (SQLException e) {
								System.out.print("You have an error in your SQL syntax, check the log");
								e.printStackTrace();
							} catch (IndexOutOfBoundsException e) {
								System.out.println("Database returned more entries than the list??? Check identifier");
								e.printStackTrace();
							}
						}while(rs.next());
					//Empty the map
					map.clear();
				}
			}
		} catch (SQLException e) {
			System.out.print("You have an error in your SQL syntax, check the log");
			e.printStackTrace();
		} catch (IndexOutOfBoundsException e) {
			System.out.println("Database returned more entries than the list??? Check identifier");
			e.printStackTrace();
		}

	}

	/**
	 * Given the value held in memory, the value held in the database, and the name of the column, this method compares the values and prints an evaluation.
	 * @param inMem The value held in memory
	 * @param inDB The value held in the database
	 * @param columnName The name of the column
	 */
	public void compareEntries(String inMem, String inDB, String columnName){
		if(inMem!=null&&inDB!=null){
			if(!inMem.equals(inDB)){
				System.out.println(String.format("Entry %s: %s %s does not match value %s in memory", pokey, columnName, inDB, inMem));
			}
			else if(inMem.equals(inDB)&&verbose){
				System.out.println(String.format("Entry %s: %s %s does match value %s in memory", pokey, columnName, inDB, inMem));
			}
		}
	}
}
