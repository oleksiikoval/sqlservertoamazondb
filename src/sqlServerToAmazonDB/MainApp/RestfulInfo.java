package sqlServerToAmazonDB.MainApp;

 

import java.util.Arrays;

import javax.xml.soap.Detail;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Class to hold information about RESTful APIs, for the purpose of making calls
 * @author Ming Quan
 *
 */
public class RestfulInfo {

 
	 
    
	
	@JsonProperty("account")
	String account;
	@JsonProperty("username")
	String username;
	@JsonProperty("type")
	String type;
	
	
	@JsonProperty("password")
	String password;
	@JsonProperty("userkey")
	String userkey;
	@JsonProperty("page")
	String page;
	@JsonProperty("chains")
	String chains;
	@JsonProperty("fulfillment_status")
	String fulfillment_status;
	@JsonProperty("restful_status")
	String restful_status;
	@JsonProperty("limit")
	String limit;
	@JsonProperty("update_time_start")
	String update_time_start;
	@JsonProperty("update_time_end")
	String update_time_end;
	@JsonProperty("soapstring")
	String soapstring;
	 
	 
	 
	@JsonProperty("bucketname")
	String bucketname;
	@JsonProperty("bucketregion")
	String bucketregion;
	
	@JsonProperty("dblink")
	String dblink;
	@JsonProperty("dbname")
	String dbname;
	@JsonProperty("dbusername") 
	String dbusername;
	@JsonProperty("dbpassword")
	String dbpassword;
	@JsonProperty("tablename")
	String tablename;
	@JsonProperty("tablename2")
	String tablename2;
	@JsonProperty("tablename3")
	String tablename3;
	@JsonProperty("soapserverurl") 
	String soapserverurl;
	@JsonProperty("soapfile") 
	String soapfile;
	@JsonProperty("soapres")
	String soapres; 
	@JsonProperty("restfulres")
	String restfulres; 
	@JsonProperty("webappId")
	String webappId;
	@JsonProperty("customId")
	String customId;
	@JsonProperty("status")
	String status; 
	@JsonProperty("sss")
	 String sss; 
	 
	@JsonProperty("detail")
	Detail detail;
	@JsonProperty("detail-type")
	String detailType;
	@JsonProperty("source")
	String source;
	@JsonProperty("time")
	String time;
	@JsonProperty("xmlfilename")
	String xmlfilename;
	@JsonProperty("id")
	String id;
	@JsonProperty("resources")
	String[] resources;
	@JsonProperty("customername")
	String customername;
	@JsonProperty("workflowname")
	String workflowname;
	@JsonProperty("sessionid")
	String sessionid;
	
	public RestfulInfo() {
    }
 
    /**
	 * @return the account
	 */
	public String getAccount() { 
		return account;
	}
 
 
	public void setAccount(String account) {
		this.account = account;
	}
	
	public String getSoapstring() { 
		return soapstring;
	}
 
 
	public void setSoapstring(String soapstring) {
		this.soapstring = soapstring;
	}
	
 
	 
	public String getUsername() {
		return username;
	}
 
 
	public void setUsername(String username) {
		this.username = username;
	}
	
	 
 
	public String getUsernam2() {
		return xmlfilename;
	}
 
 
	public void setUsernam2(String xmlfilename) {
		this.xmlfilename = xmlfilename;
	}
	
	public String getPassword() {
		return password;
	}
 
 
	public void setPassword(String password) {
		this.password = password;
	}
 
	public String getUserkey() {
		return userkey;
	}
 
 
	public void setPages(String page) {
		this.page = page;
	}
	
	public String getPages() {
		return page;
	}
 
	public void setUpdatetime_start(String update_time_start) {
		this.update_time_start = update_time_start;
	}
	
	public String getUpdatetime_start() {
		return update_time_start;
	}
 
	
	public void setUpdatetime_end(String update_time_end) {
		this.update_time_end =update_time_end;
	}
	
	public String getUpdatetime_end() {
		return update_time_end;
	}
	public void setUserkey(String userkey) {
		this.userkey = userkey;
	}
	
	public String getWebappId() {
		return webappId;
	}
 
 
	public void setWebappId(String webappId) {
		this.webappId = webappId;
	}
	public String getCustomId() {
		return webappId;
	}
 
 
	public void setCustomId(String webappId) {
		this.webappId = webappId;
	}
	
	
	public String getChains() {
		return chains;
	}
 
 
	public void setChains(String chains) {
		this.chains = chains;
	}
	

 
	public String getBucketname() {
		return bucketname;
	}
 
 
	public void setBucketname(String bucketname) {
		this.bucketname = bucketname;
	}
	
 
	
	public String getDbname() {
		return dbname;
	}
 
 
	public void setDbname(String dbname) {
		this.dbname = dbname;
	}
	
	public String getDblink() {
		return dblink;
	}
 
 
	public void setDblink(String dblink) {
		this.dblink = dblink;
	}
	
	public String getBucketregion() {
		return bucketregion;
	}
 
 
	public void setBucketregion(String bucketregion) {
		this.bucketregion = bucketregion;
	}
	
	
 
 
	public String getDbusername() {
		return dbusername;
	}
 
	public void setDbusername(String dbusername) {
		this.dbusername = dbusername;
	}
	public String getDbpassword() {
		return dbpassword;
	}
 
 
	public void setDbpassword(String dbpassword) {
		this.dbpassword = dbpassword;
	}
	
	public String getTablename() {
		return tablename;
	}
 
 
	public void setTablename(String tablename) {
		this.tablename = tablename;
	}
	
	public String getTablename2() {
		return tablename2;
	}
 
 
	public void setTablename2(String tablename2) {
		this.tablename2 = tablename2;
	}
	 
	public String getTablename3() {
		return tablename3;
	}
 
 
	public void setTablename3(String tablename3) {
		this.tablename3 = tablename3;
	}
	 
		       
	public String getType() {
		return type;
	}

	public void setType(String Type) {
		this.type = Type;
	}
 
	 
 
 
	@JsonProperty("detail")
	public Detail getDetail() {
		return detail;
	}
 
 
	@JsonProperty("detail")
	public void setDetail(Detail detail) {
		this.detail = detail;
	}
	 

 
	public  String getFulfillment_status() {
		return fulfillment_status ;
	}
 
 
 
	public void setFulfillment_status(String fulfillment_status) {
		this.fulfillment_status = fulfillment_status;
	}
 
 
	 
	public  String getRestful_statu() {
		return restful_status ;
	}
 
 
	@JsonProperty("restful_status")
	public void setRestful_statu(String restful_status) {
		this.restful_status = restful_status;
	}
	
	 
	public  String getLimit() {
		return limit ;
	}
 
 
 
	public void setLimit(String limit) {
		this.limit = limit;
	}
	
	public String getDetailType() {
		return detailType;
	}
 
	 
	public void setDetailType(String detailType) {
		this.detailType = detailType;
	}
 
 
	public String getSource() {
		return source;
	}
	
	public void setSource(String soruce) {
		this.source = soruce;
	}
 
	public String getSoapfile() {
		return soapfile;
	}
 
 
 
	public void setSoapfile(String soapfile) {
		this.soapfile = soapfile;
	}
 
	public String getSoapserverurl() {
		return soapserverurl;
	}
 
 
	public void setSoapserverurl(String soapserverurl) {
		this.soapserverurl = soapserverurl;
	}
 
	public void setSoapServerUrl(String soapserverurl) {
		this.soapserverurl = soapserverurl;
	}
 
	public void setSoapres(String soapres) {
		this.soapres = soapres;
	}
 
	public String getSoapres() {
		return soapres;
	}
 
	public void setRestfulres(String restfulres) {
		this.restfulres = restfulres;
	}
 
	public String getRestfulres() {
		return restfulres;
	}
 

 
	public String getStatus() {
		return status;
	}
 
 
	public void setSuccessorfail(String sss) {
		this.sss = sss;
	}
	
	public String getSuccessorfail() {
		return sss;
	}
 
 
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getTime() {
		return time;
	}
 
 
	public void setTime(String time) {
		this.time = time;
	}
	
   
 
 
	public String getId() {
		return id;
	}
 
 
	public void setId(String id) {
		this.id = id;
	}
 
 
	public String[] getResources() {
		return resources;
	}
 
 
	public void setResources(String[] resources) {
		this.resources = resources;
	}
	
	public String getCustomername() {
		return customername;
	}
 
 
	public void setCustomername(String customername) {
		this.customername = customername;
	} 
 
	public String getWorkflowname() {
		return workflowname;
	}
 
 
	public void setworkflowname(String workflowname) {
		this.workflowname = workflowname;
	} 
	
	public String getSessionid() {
		return sessionid;
	}
 
 
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	} 
  
    @Override
    public String toString() {
        return "Shopify Configuration Paremeters{" +
                "account='" + account + '\'' +
                ", username'" + username + '\''+
                ", password'" + password + '\''+
                ", userkey'" + userkey +'\'' +
                ", status'" + status +'\'' +
                ", update_time_start" + '\'' + update_time_start +
                ", update_time_end" + '\'' + update_time_end +
                ", bucketname='"+ bucketname+'\''+
                ", fullfillment_status='"+ fulfillment_status+'\''+
                 ", restful_status='"+ restful_status+'\''+
                 ",limit='"+ limit+'\''+
                  ",page='"+ page+'\''+
                ", bucketregion='" + bucketregion+'\''+
               // ", soapserverurl'" + soapserverurl+'\''+
                ", detail='" + detail + '\'' +
                ", soapfile='" + soapfile + '\'' +  
                 ", dblink='" + dblink + '\'' +    
                  ", dbname='" + dbname + '\'' +
                  ", dbusername='" + dbusername + '\'' +
                 ", dbpassword='" + dbpassword + '\'' +
                ", dbtoxmlfilename='" + xmlfilename + '\'' +
                    ", tablename='" + tablename + '\'' +  
                          ", tablename2='" + tablename2 + '\'' +  
                         ", tablename3= '"+ tablename3+'\''+
               // ", soapres='" + soapres + '\''+
                     ", restfulres='" + restfulres + '\''+
                ", detailType='" + detailType + '\'' +  
                ", source='" + source + '\'' +
                ", time='" + time + '\'' +    
                ", id='" + id + '\'' +
                ", sss='" + sss +'\''+
                ", resources=" + Arrays.toString(resources) +
                '}';
    }
} 
